package org.example.receiver.controller;

import org.example.receiver.model.Notification;
import org.example.receiver.service.ReceiverServiceImpl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReceiverController {

    private final RabbitTemplate rabbitTemplate;
    private final ReceiverServiceImpl receiverService;

    public ReceiverController(RabbitTemplate rabbitTemplate, ReceiverServiceImpl receiverService) {
        this.rabbitTemplate = rabbitTemplate;
        this.receiverService = receiverService;
    }

    @GetMapping("/message")
    public String receiveMessage() {

        return receiverService.receiveNotification();
    }

    @RabbitListener(queues = "test_queue")
    public void listenerMessage(Notification notification){
        System.out.println("Message: "+notification.toString());
    }

    @GetMapping("/notification")
    public ResponseEntity<Notification> listenerNotification(){
        Notification notification = rabbitTemplate.receiveAndConvert("test_queue", ParameterizedTypeReference.forType(Notification.class));
        if(notification !=null){
            return ResponseEntity.ok(notification);
        }
        return ResponseEntity.noContent().build();
    }
}

package org.example.receiver.service;


import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReceiverServiceImpl {

    private final RabbitTemplate rabbitTemplate;


    public ReceiverServiceImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;

    }

    public String receiveNotification()
    {
        Object message = rabbitTemplate.receiveAndConvert("test_queue");
        if(message!=null)
        {
            return "Received message with success: " + message.toString();
        }
        else{
            return "Empty queue";
        }
    }
}

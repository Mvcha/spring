package org.example.publisher.service;

import org.example.publisher.entity.Status;
import org.example.publisher.entity.Student;

import java.util.List;
import java.util.Optional;


public interface StudentService {

    void saveStudent(Student student);

    void deleteStudent(Long id);

    Student findStudentById(Long studentId);

    Student putStudent(Long id, Student student);

    Student patchStudent(Long id, Student student);

    boolean dbIsEmpty();

    List<Student> findAllStudents();

    List<Student> findStudentsByStatus(Status status);


}

package org.example.publisher.repository;

import org.example.publisher.entity.Status;
import org.example.publisher.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Boolean existsByEmail(String email);
    List<Student> findStudentByStatus(Status status);
}

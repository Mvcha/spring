package org.example.publisher.service;

import org.example.publisher.entity.Status;
import org.example.publisher.entity.Student;
import org.example.publisher.exception.StudentError;
import org.example.publisher.exception.StudentException;
import org.example.publisher.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;



@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void saveStudent(Student student) {
        validateIfEmailExists(student);
        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentException(StudentError.STUDENT_NOT_FOUND));

        student.setStatus(Status.INACTIVE);
        patchStudent(id, student);
    }

    public Student findStudentById(Long studentId) {
        if (studentRepository.findById(studentId).isPresent()) {
            Student student = studentRepository.findById(studentId).get();
            if(student.getStatus()==Status.INACTIVE){
                throw new StudentException(StudentError.STUDENT_IS_INACTIVE);
            }
            return student;
        } else {
            throw new StudentException(StudentError.STUDENT_NOT_FOUND);
        }
    }

    @Override
    public Student putStudent(Long id, Student student) {
        return studentRepository.findById(id)
                .map(studentFromDb ->{
                    validateIfEmailExists(student);
                    studentFromDb.setFirstName(student.getFirstName());
                    studentFromDb.setLastName(student.getLastName());
                    studentFromDb.setEmail(student.getEmail());
                    return studentRepository.save(studentFromDb);
                }).orElseThrow(() ->new StudentException(StudentError.STUDENT_NOT_FOUND));
    }

    private void validateIfEmailExists(Student student) {
        if(studentRepository.existsByEmail(student.getEmail())) {
            throw new StudentException(StudentError.EMAIL_IS_EXISTS);
        }
    }

    @Override
    public Student patchStudent(Long id, Student student) {
        return studentRepository.findById(id)
                .map(studentFromDb ->{
                    if(!StringUtils.hasLength(student.getFirstName())){
                        studentFromDb.setFirstName(student.getFirstName());
                    }
                    if(!StringUtils.hasLength(student.getLastName())){
                        studentFromDb.setLastName(student.getLastName());
                    }
                    return studentRepository.save(studentFromDb);
                }).orElseThrow(()->new StudentException(StudentError.STUDENT_NOT_FOUND));
    }

    public boolean dbIsEmpty() {
        if (findAllStudents().isEmpty()) {
            return true;
        }
        return false;
    }


    @Override
    public List<Student> findStudentsByStatus(Status status) {
        return studentRepository.findStudentByStatus(status);
    }

    public List<Student> findAllStudents() {
        return studentRepository.findAll();
    }

}

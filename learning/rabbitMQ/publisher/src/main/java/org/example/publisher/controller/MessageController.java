package org.example.publisher.controller;

import jakarta.validation.Valid;
import org.example.publisher.entity.Student;
import org.example.publisher.model.Notification;
import org.example.publisher.service.PublisherService;
import org.example.publisher.service.StudentServiceImpl;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {

    private final RabbitTemplate rabbitTemplate;
    private final PublisherService publisherService;
    public MessageController(RabbitTemplate rabbitTemplate, PublisherService publisherService, StudentServiceImpl studentService) {
        this.rabbitTemplate = rabbitTemplate;
        this.publisherService = publisherService;
    }


    @GetMapping("/message")
    public String sendMessage(@RequestParam String message){
        rabbitTemplate.convertAndSend("test_queue", message);
        return "Show message RabbitMQ";
    }

    @PostMapping("/notification")
    public String sendNotification(@RequestBody Notification notification){
        rabbitTemplate.convertAndSend("test_queue", notification);

        return "Notification sent";
    }

    @GetMapping("/notifications")
    public String sentNotificationForStudent(@RequestParam Long studentId)
    {
        publisherService.publicNotificationAboutStudent(studentId);

        return "Notification sent with student data";

    }

}

package org.example.publisher.exception;

public enum StudentError {
    STUDENT_NOT_FOUND("Student doesn't exists"),
    EMAIL_IS_EXISTS("This email already exists"),
    STUDENT_IS_INACTIVE("This student is inactive");


    private String message;

    StudentError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}

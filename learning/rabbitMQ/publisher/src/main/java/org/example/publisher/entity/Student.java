package org.example.publisher.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import jdk.jshell.Snippet;


@Entity
@SequenceGenerator(name = "seqIdGen", initialValue = 20000, allocationSize = 1)
public class Student {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "seqIdGen")
    private Long id;
    @NotBlank
    private String firstName;
    @NotEmpty
    @Size(min = 3)
    private String lastName;
    @NotBlank
    @Email
    private String email;
    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Student() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Student(String email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(String email, String firstName, String lastName, Status status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.status = status;
    }
}

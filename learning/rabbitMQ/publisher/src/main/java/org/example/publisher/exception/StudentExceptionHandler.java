package org.example.publisher.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class StudentExceptionHandler {

    @ExceptionHandler(value = StudentException.class)
    public ResponseEntity<ErrorInfo> handleException(StudentException e) {


        switch (e.getStudentError()) {
            case STUDENT_NOT_FOUND:
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorInfo(e.getStudentError().getMessage()));
            case EMAIL_IS_EXISTS:
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorInfo(e.getStudentError().getMessage()));
            case STUDENT_IS_INACTIVE:
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorInfo(e.getStudentError().getMessage()));

            default:
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorInfo("Unknown error"));
        }

    }
}

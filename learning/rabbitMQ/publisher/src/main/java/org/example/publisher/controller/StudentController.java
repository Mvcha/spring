package org.example.publisher.controller;

import jakarta.validation.Valid;
import org.example.publisher.entity.Status;
import org.example.publisher.entity.Student;
import org.example.publisher.service.PublisherService;
import org.example.publisher.service.StudentServiceImpl;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {


    private StudentServiceImpl studentService;

    public StudentController( StudentServiceImpl studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/fill")
    public String createStudents() {
        if (studentService.dbIsEmpty() == true) {

            studentService.saveStudent(new Student("jan@gmail.com", "Jan", "Nowak", Status.INACTIVE));
            studentService.saveStudent(new Student("michael@gmail.com", "Michael", "Owen",Status.ACTIVE));
            studentService.saveStudent(new Student("zofia@gmail.com", "Zofia", "Nowak", Status.ACTIVE));
            studentService.saveStudent(new Student("john@gmail.com", "John", "Boris",Status.ACTIVE));
            studentService.saveStudent(new Student("peter@gmail.com", "Peter", "Borovski",Status.ACTIVE));
            return "Added data";
        }
        return "DB was filled";
    }

    @PostMapping("/add")
    public void  addStudent(@Valid @RequestBody Student student) {
        studentService.saveStudent(student);
    }

    @GetMapping("")
    public List<Student> getStudents(@RequestParam(required = false) Status status){
        if(status==null)
        {
            return studentService.findAllStudents();
        }
        else {
            return studentService.findStudentsByStatus(status);
        }
    }

    @GetMapping("/{studentId}")
    public Student getStudent(@PathVariable Long studentId){
        return studentService.findStudentById(studentId);
    }


    @DeleteMapping("/{studentId}")
    public void deleteStudent(@PathVariable Long studentId){
        studentService.deleteStudent(studentId);
    }
    @PutMapping("/{studentId}")
    public void putStudent(@PathVariable Long studentId, @Valid @RequestBody Student student){
        studentService.putStudent(studentId, student);
    }
}

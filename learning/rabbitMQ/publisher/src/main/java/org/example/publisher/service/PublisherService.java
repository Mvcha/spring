package org.example.publisher.service;

import org.example.publisher.entity.Student;
import org.example.publisher.model.Notification;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PublisherService {

    private final RabbitTemplate rabbitTemplate;
    private final RestTemplate restTemplate;
    private final StudentServiceImpl studentService;

    public PublisherService(RabbitTemplate rabbitTemplate, RestTemplate restTemplate, StudentServiceImpl studentService) {
        this.rabbitTemplate = rabbitTemplate;
        this.restTemplate = restTemplate;
        this.studentService = studentService;
    }

    public void publicNotificationAboutStudent(Long studentId){
        Student student=restTemplate.exchange("http://localhost:8080/students/"+studentId,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                Student.class).getBody();
        Notification notification=new Notification(student.getEmail(),
                "Hello "+student.getFirstName(),
                "I'm a glad that you are with us "+student.getFirstName()+" "+student.getLastName());

        rabbitTemplate.convertAndSend("test_queue", notification);

    }
}

package org.example.resourceserver.controller;

import org.example.resourceserver.model.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ProductController {

    private static List<Product> productList = new LinkedList<>(List.of(
            Product.builder().id("001").name("Black T-Shirt").build(),
            Product.builder().id("002").name("Blue Jacket").build()
    ));


    @GetMapping("/product")
    public ResponseEntity<List<Product>> getProduct(){
        return ResponseEntity.ok(productList);
    }

    @PostMapping("/product")
    public ResponseEntity<Void> getProduct(@RequestBody Product product){
        productList.add(product);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<String> index(){
        return ResponseEntity.ok().body("Endpoint without authorization");
    }
}

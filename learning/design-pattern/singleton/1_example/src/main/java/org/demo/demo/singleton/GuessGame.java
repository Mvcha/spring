package org.demo.demo.singleton;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {

    private int score;
    private Scanner scanner = new Scanner(System.in);
    private Random random = new Random();

    private static GuessGame guessGame = new GuessGame();

    public void play() {
        for (int i = 1; i < 11; i++) {
            System.out.println("Round number: " + i);
            System.out.println("--------------------------");

            System.out.println("Guess a digit from 0 to 9 \n");
            int randomDigit=random.nextInt(9 - 0);
            if (scanner.nextInt() ==randomDigit ) {
                System.out.println("Lucky punch! \n");
                score++;
            }
            else {
                System.out.println("Wrong digit. Good digit was: "+randomDigit);
            }

        }
        System.out.println("\n");
        System.out.println("Your score is: "+score);

    }

    public int getScore() {
        return score;
    }

    public static GuessGame getInstance() {
        if (guessGame == null) {
            guessGame = new GuessGame();
        }
        return guessGame;
    }
    protected Object readResolve() {
        return getInstance();
    }
}

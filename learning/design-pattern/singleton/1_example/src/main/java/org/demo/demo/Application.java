package org.demo.demo;

import org.demo.demo.singleton.GameEngine;
import org.demo.demo.singleton.GuessGame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);


        GameEngine engine=GameEngine.getInstance();
        GameEngine engine2= GameEngine.getInstance();
        System.out.println(engine==engine2);


        GuessGame game = GuessGame.getInstance();

        game.play();

        int score = game.getScore();

        GuessGame anotherGameReference = GuessGame.getInstance();

        if(game == anotherGameReference  ) {
            System.out.println("Singleton!");
            if(score == anotherGameReference.getScore()) {
                System.out.println("Score is correct!");
            }
        }
    }
}


package org.demo.house.classicVersion;

public interface HouseBuilder2 {
    void buildWalls();
    void buildFloors();
    void buildRooms();
    void buildRoof();
    void buildGarage();
    void buildDoors();
    void buildWindows();
    House2 getHouse();
}

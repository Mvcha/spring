package org.demo;

import org.demo.house.House;
import org.demo.house.classicVersion.BigHouseBuilder;
import org.demo.house.classicVersion.House2;
import org.demo.house.classicVersion.HouseDirector;
import org.demo.house.classicVersion.SmallHouseBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

/*
        House house1=new House("walls", "floors", "rooms", "windows", "doors", "garage");
        House house2=new House()
*/
        System.out.println(" 1 Example\n");
        House house=new House.HouseBuilder()
                .buildWalls("walls")
                .buildFloors("floors")
                .buildRoof("roof")
                .buildRooms("rooms")
                .build();
        System.out.println(house);

        System.out.println("-------------------------------");
        System.out.println(" 2 Example\n");
        SmallHouseBuilder smallHouseBuilder=new SmallHouseBuilder();
        BigHouseBuilder bigHouseBuilder=new BigHouseBuilder();

        HouseDirector smallHouseDirector = new HouseDirector(smallHouseBuilder);
        smallHouseDirector.buildHouse2();

        HouseDirector bigHouseDirector = new HouseDirector(bigHouseBuilder);
        bigHouseDirector.buildHouse2();

        House2 smallHouse = smallHouseDirector.getHouse();
        House2 bigHouse = bigHouseDirector.getHouse();

        System.out.println("Small house:");
        System.out.println(smallHouse);
        System.out.println("Big house:");
        System.out.println(bigHouse);
}}

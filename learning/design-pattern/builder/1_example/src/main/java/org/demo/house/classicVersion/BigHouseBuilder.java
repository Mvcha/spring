package org.demo.house.classicVersion;

public class BigHouseBuilder implements HouseBuilder2{

    private  House2 house2;

    public BigHouseBuilder() {
        this.house2=new House2();
    }

    @Override
    public void buildWalls() {
        this.house2.setWalls("big walls");
    }

    @Override
    public void buildFloors() {
        this.house2.setFloors("big floors");
    }

    @Override
    public void buildRooms() {
        this.house2.setRooms("big rooms");
    }

    @Override
    public void buildRoof() {
        this.house2.setRoof("big roof");
    }

    @Override
    public void buildGarage() {
        this.house2.setGarage("big garage");
    }

    @Override
    public void buildDoors() {
        this.house2.setDoors("big doors");
    }

    @Override
    public void buildWindows() {
        this.house2.setWindows("big windows");
    }

    @Override
    public House2 getHouse() {
        return house2;
    }
}

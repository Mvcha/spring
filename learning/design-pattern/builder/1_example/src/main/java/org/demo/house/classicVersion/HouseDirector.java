package org.demo.house.classicVersion;

public class HouseDirector {
    private HouseBuilder2 houseBuilder2;
    public HouseDirector(HouseBuilder2 houseBuilder2) {
        this.houseBuilder2=houseBuilder2;
    }


    public void buildHouse2(){
        houseBuilder2.buildDoors();
        houseBuilder2.buildWalls();
        houseBuilder2.buildRoof();
        houseBuilder2.buildFloors();
        houseBuilder2.buildRooms();
        houseBuilder2.buildFloors();
        houseBuilder2.buildGarage();
    }

    public House2 getHouse() {
        return this.houseBuilder2.getHouse();
    }
}

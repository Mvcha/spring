package org.demo.house.classicVersion;

public class SmallHouseBuilder implements HouseBuilder2{

    private  House2 house2;

    public SmallHouseBuilder() {
        this.house2=new House2();
    }

    @Override
    public void buildWalls() {
        this.house2.setWalls("small walls");
    }

    @Override
    public void buildFloors() {
        this.house2.setFloors("small floors");
    }

    @Override
    public void buildRooms() {
        this.house2.setRooms("small rooms");
    }

    @Override
    public void buildRoof() {
        this.house2.setRoof("small roof");
    }

    @Override
    public void buildGarage() {
        this.house2.setGarage("small garage");
    }

    @Override
    public void buildDoors() {
        this.house2.setDoors("small doors");
    }

    @Override
    public void buildWindows() {
        this.house2.setWindows("small windows");
    }

    @Override
    public House2 getHouse() {
        return house2;
    }
}

package org.demo.builder.flightLeg;

import lombok.ToString;

@ToString
public class FlightLeg {

    private String from;
    private String to;
    private Boolean delayed;
    private int price;


    private FlightLeg(FlightLegBuilder flightLegBuilder)
    {
        from=flightLegBuilder.from;
        to=flightLegBuilder.to;
        delayed=flightLegBuilder.delayed;
        price=flightLegBuilder.price;
    }

    public static class FlightLegBuilder{
        private final String from;
        private final String to;
        private final Boolean delayed;
        private int price;

        public FlightLegBuilder(String from, String to) {
            this.from = from;
            this.to = to;
            this.delayed = false;
        }

        public FlightLegBuilder  price(int price) {
            this.price = price;
            return this;
        }

        public FlightLeg build(){
            if(this.price==0) {
                throw new IllegalStateException("Required field missing ");
            }
            return new FlightLeg(this);
        }
    }
}

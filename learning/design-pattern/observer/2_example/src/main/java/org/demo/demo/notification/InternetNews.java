package org.demo.demo.notification;

import org.demo.demo.weather.WeatherForecast;

public class InternetNews implements Observer{
    @Override
    public void updateForecast(WeatherForecast weatherForecast) {
        System.out.println(getClass().getSimpleName() +" new weather forecast: temperature "
                +weatherForecast.getTemperature()+ " pressure "+weatherForecast.getPressure());
    }
}

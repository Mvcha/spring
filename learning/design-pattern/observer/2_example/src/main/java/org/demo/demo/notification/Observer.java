package org.demo.demo.notification;

import org.demo.demo.weather.WeatherForecast;

public interface Observer {

    void  updateForecast(WeatherForecast weatherForecast);
}

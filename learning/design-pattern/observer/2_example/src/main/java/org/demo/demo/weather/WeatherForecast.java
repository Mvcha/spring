package org.demo.demo.weather;

import lombok.Getter;
import lombok.Setter;
import org.demo.demo.notification.Observer;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class WeatherForecast implements Observable {
    private int temperature;
    private int pressure;
    private Set<Observer> registeredObservers = new HashSet<Observer>();

    public WeatherForecast(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
    }

    @Override
    public void registerObserver(Observer observer) {
        registeredObservers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        registeredObservers.remove(observer);

    }

    @Override
    public void notifyObservers() {
        registeredObservers.forEach(observer -> observer.updateForecast(this));
    }

    public void updateForecast(int temperature, int pressure){
        setTemperature(temperature);
        setPressure(pressure);
        notifyObservers();
    }
}

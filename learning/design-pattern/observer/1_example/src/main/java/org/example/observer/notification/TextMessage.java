package org.example.observer.notification;

import org.example.observer.order.Order;

public class TextMessage implements Observer {
    @Override
    public void update(Order order){
        System.out.println("SMS - The order with number: "+ " changed status to "+order.getOrderStatus());
    }

}

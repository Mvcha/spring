package org.example.observer.notification;

import org.example.observer.order.Order;

public interface Observer {
    void update(Order order);
}

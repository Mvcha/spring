package org.example.observer.notification;

import org.example.observer.order.Order;

public class Email implements Observer{

    @Override
    public void update(Order order) {
        System.out.println("Email - The order with number: "+ " changed status to "+order.getOrderStatus());
    }
}

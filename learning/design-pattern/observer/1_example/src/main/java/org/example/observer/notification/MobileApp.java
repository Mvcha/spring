package org.example.observer.notification;

import org.example.observer.order.Order;

public class MobileApp implements Observer {
    @Override
    public void update(Order order){
        System.out.println("Mobile App - The order with number: "+ " changed status to "+order.getOrderStatus());
    }

}

package org.example.observer.order;

public enum OrderStatus {
    REGISTERED,
    SENT,
    RECEIVED
}

package org.example.observer;

import org.example.observer.notification.Email;
import org.example.observer.notification.MobileApp;
import org.example.observer.notification.TextMessage;
import org.example.observer.order.Order;
import org.example.observer.order.OrderStatus;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObserverApplication.class, args);

        Order order = new Order(110L, OrderStatus.REGISTERED);

        MobileApp mobileApp=new MobileApp();
        Email email= new Email();
        TextMessage textMessage=new TextMessage();

        order.registerObserver(textMessage);
        order.registerObserver(mobileApp);
        order.registerObserver(email);
        order.notifyObservers();
        System.out.println("------------");
        order.changeOrderStatus(OrderStatus.SENT);
    }

}

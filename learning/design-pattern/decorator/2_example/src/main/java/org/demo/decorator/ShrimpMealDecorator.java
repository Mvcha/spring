package org.demo.decorator;

public class ShrimpMealDecorator extends MealDecorator{

    public ShrimpMealDecorator(Meal decoratedMeal) {
        super(decoratedMeal);
    }
    @Override
    public void prepareMeal() {
        meal.prepareMeal();
        addShrimp();
    }
    public void addShrimp(){
        System.out.println("Added shrimp to the meal");
    }
}

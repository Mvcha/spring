package org.demo.decorator;

public class RiceMeal extends Meal{

    @Override
    public void prepareMeal() {
        System.out.println("I'm preparing meal which is based on rice");

    }
}

package org.demo.decorator;

public abstract class Meal {

    public void prepareMeal() {
        System.out.println("I'm preparing a meal");
    }
}

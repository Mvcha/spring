package org.demo.chainofresponsibility.kids;

import org.demo.chainofresponsibility.MotherRequest;
import org.demo.chainofresponsibility.Shelf;

public class Tom extends Child{
    @Override
    public void processRequest(MotherRequest motherRequest) {
        if (motherRequest.getShelf().equals(Shelf.MEDIUM)) {
            System.out.println("Tom took the jar off the shelf ");
        } else {
            getTallerChild().processRequest(motherRequest);
        }
    }
}

package org.demo.chainofresponsibility.kids;

import org.demo.chainofresponsibility.MotherRequest;
import org.demo.chainofresponsibility.Shelf;

public class John extends Child{
    @Override
    public void processRequest(MotherRequest motherRequest) {
        if (motherRequest.getShelf().equals(Shelf.HIGH)) {
            System.out.println("John took the jar off the shelf ");
        } else {
            getTallerChild().processRequest(motherRequest);
        }
    }
}

package org.demo.chainofresponsibility;

import org.demo.chainofresponsibility.kids.Anna;
import org.demo.chainofresponsibility.kids.Child;
import org.demo.chainofresponsibility.kids.John;
import org.demo.chainofresponsibility.kids.Tom;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        MotherRequest motherRequest = new MotherRequest(Shelf.HIGH);
        Child tom = new Tom();
        Child anna = new Anna();
        Child john = new John();
        tom.setTallerChild(anna);
        anna.setTallerChild(john);
        tom.processRequest(motherRequest);
    }

}

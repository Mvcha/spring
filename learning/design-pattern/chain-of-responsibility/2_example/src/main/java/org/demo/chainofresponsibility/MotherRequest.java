package org.demo.chainofresponsibility;

public class MotherRequest {
    private Shelf shelf;

    public Shelf getShelf() {
        return shelf;
    }

    public MotherRequest(Shelf shelf) {
        this.shelf = shelf;
    }
}

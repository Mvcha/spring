package org.demo.chainofresponsibility;

public enum Shelf {
    LOW,
    MEDIUM,
    HIGH
}

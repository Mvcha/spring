package org.demo.chainofresponsibility.kids;

import org.demo.chainofresponsibility.MotherRequest;
import org.demo.chainofresponsibility.Shelf;

public class Anna extends Child{

    @Override
    public void processRequest(MotherRequest motherRequest) {
        if (motherRequest.getShelf().equals(Shelf.LOW)) {
            System.out.println("Ania took the jar off the shelf ");
        } else {
            getTallerChild().processRequest(motherRequest);
        }
    }
}

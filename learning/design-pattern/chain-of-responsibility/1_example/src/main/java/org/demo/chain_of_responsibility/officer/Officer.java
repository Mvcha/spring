package org.demo.chain_of_responsibility.officer;

import org.demo.chain_of_responsibility.message.Message;

public abstract class Officer {

    public abstract void processMessage(Message message);
    private Officer superiorOfficer;

    public Officer getSuperiorOfficer() {
        return superiorOfficer;
    }

    public void setSuperiorOfficer(Officer superiorOfficer) {
        this.superiorOfficer = superiorOfficer;
    }
}


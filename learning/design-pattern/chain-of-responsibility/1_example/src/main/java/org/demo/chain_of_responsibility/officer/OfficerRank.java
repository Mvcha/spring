package org.demo.chain_of_responsibility.officer;

public enum OfficerRank {
    SERGEANT,
    CAPTAIN,
    GENERAL
}

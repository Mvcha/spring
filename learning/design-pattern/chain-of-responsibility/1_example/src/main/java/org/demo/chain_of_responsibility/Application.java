package org.demo.chain_of_responsibility;

import org.demo.chain_of_responsibility.message.Message;
import org.demo.chain_of_responsibility.officer.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        Message message = new Message("Attack", 50, OfficerRank.CAPTAIN);
        Officer sergeant = new Sergeant();
        Officer captain = new Captain();
        Officer general = new General();
        sergeant.setSuperiorOfficer(captain);
        captain.setSuperiorOfficer(general);

        sergeant.processMessage(message);
    }

}

package org.demo.chain_of_responsibility.officer;

import org.demo.chain_of_responsibility.message.Message;

public class General extends Officer{

    private static final int CODE = 100;
    private static final String NAME = "General Zabor";

    @Override
    public void processMessage(Message message) {
        if(message.getOfficerRank().equals(OfficerRank.GENERAL)
                && message.getCode()== CODE){
            System.out.println(NAME + " received message: "+message.getContent());
        }else {
            System.out.println("Bad message receiver  or bad code");
        }
    }
}

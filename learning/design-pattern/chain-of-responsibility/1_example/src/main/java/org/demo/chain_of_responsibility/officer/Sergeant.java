package org.demo.chain_of_responsibility.officer;

import org.demo.chain_of_responsibility.message.Message;

public class Sergeant extends Officer{

    private static final int CODE = 10;
    private static final String NAME = "Sergeant Johnson";
    @Override
    public void processMessage(Message message) {
        if(message.getOfficerRank().equals(OfficerRank.SERGEANT)
        && message.getCode()== CODE){
            System.out.println(NAME + " received message: "+message.getContent());
        }else {
            getSuperiorOfficer().processMessage(message);
        }
    }
}

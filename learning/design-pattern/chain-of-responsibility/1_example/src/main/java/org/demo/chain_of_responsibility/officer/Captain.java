package org.demo.chain_of_responsibility.officer;

import org.demo.chain_of_responsibility.message.Message;

public class Captain extends Officer{

    private static final int CODE = 50;
    private static final String NAME = "Captain Rock";
    @Override
    public void processMessage(Message message) {
        if(message.getOfficerRank().equals(OfficerRank.CAPTAIN)
                && message.getCode()== CODE){
            System.out.println(NAME + " received message: "+message.getContent());
        }else {
            getSuperiorOfficer().processMessage(message);
        }
    }
}
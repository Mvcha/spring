package org.demo.templatemethod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        WeekDay day = new MyDay();
        day.everyDayIsExactlyTheSame(TransportType.BIKE);
    }

}

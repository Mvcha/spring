package org.demo.templatemethod;

public enum TransportType {
    CAR,
    TRAM,
    BIKE
}

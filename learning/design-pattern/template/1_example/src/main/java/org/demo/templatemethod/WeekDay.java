package org.demo.templatemethod;

public abstract class WeekDay {

    public final void everyDayIsExactlyTheSame(TransportType transportType)
    {
        wakeUp();
        getReady();
        int time = goToWork(transportType);
        summary(time);
        work();
        goHome();
    }

    private void summary(int time) {
        System.out.println("The route took "+time+" minutes");
    }

    private void goHome() {
        System.out.println("I'm going back to the home");
    }

    protected abstract void work();

    protected abstract int goToWork(TransportType transportType);

    private void getReady() {
        System.out.println("I'm preparing to go out");
    }

    private void wakeUp() {
        System.out.println("Wake up");
        
    }
}

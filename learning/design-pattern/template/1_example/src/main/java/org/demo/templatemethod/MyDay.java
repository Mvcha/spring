package org.demo.templatemethod;

public class MyDay extends WeekDay{
    @Override
    protected void work() {
        System.out.println("Work, work, work");
    }

    @Override
    protected int goToWork(TransportType transportType) {
        switch(transportType) {
            case CAR:
                return 15;
            case BIKE:
                return 25;
            case TRAM:
                return 20;
            default:
                return 30;
        }
    }
}

package org.demo.adapter;

public class TwoWayAdapter implements UKDevice, ContinentalDevice {
    UKDevice ukDevice;
    ContinentalDevice device;

    public TwoWayAdapter(UKDevice ukDevice, ContinentalDevice device) {
        this.ukDevice = ukDevice;
        this.device = device;
    }

    @Override
    public void on() {
        ukDevice.on();
    }

    @Override
    public void powerOn() {
        device.powerOn();
    }
}

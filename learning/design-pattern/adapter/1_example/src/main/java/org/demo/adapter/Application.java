package org.demo.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        ContinentalDevice radio=new ContinentalDevice() {
            @Override
            public void powerOn() {
                System.out.println("Playing music");
            }
        };

        ContinentalSocket continentalSocket=new ContinentalSocket();
        continentalSocket.plugIn(radio);

        UKDevice ukRadio=new UKDevice() {
            @Override
            public void on() {
                System.out.println("London music");
            }
        };

        ContinentalDevice continentalRadio=new ContinentalDevice() {
            @Override
            public void powerOn() {
                System.out.println("Continental radio...");
            }
        };
        UKSocket ukSocket=new UKSocket();
        ukSocket.plugIn(ukRadio);

        UKToContinentalAdapter adapter=new UKToContinentalAdapter(ukRadio);
        continentalSocket.plugIn(adapter);


        System.out.println("---------");
        TwoWayAdapter adapter2=new TwoWayAdapter(ukRadio,continentalRadio);
        continentalSocket.plugIn(adapter2);
        ukSocket.plugIn(adapter2);
    }

}

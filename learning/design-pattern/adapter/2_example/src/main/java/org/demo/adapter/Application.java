package org.demo.adapter;

import org.demo.adapter.api.LibraryAPI;
import org.demo.adapter.api.LibraryAPIImpl;
import org.demo.adapter.api.LibraryAPIv2;
import org.demo.adapter.api.LibraryAPIv2Impl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
       /* SpringApplication.run(Application.class, args);
        LibraryAPI api = new LibraryAPIImpl();
        User user = new User("Paweł","Cwik","32131212");
        BookConnector connector = new BookConnector(user,api);
        connector.checkAvailability("Harry Potter i Zakon Feniksa");*/


        User user = new User("Paweł","Cwik","32131212");
        LibraryAPIv2 libraryAPIv2 = new LibraryAPIv2Impl();
        APIAdapter apiAdapter = new APIAdapter(libraryAPIv2, user);
        BookConnector connector2 = new BookConnector(user,apiAdapter);
        connector2.checkAvailability("Harry Potter i Zakon Feniksa");
    }

}

package org.demo.factory.units;

public class Rifleman extends InfantryUnit {

    protected Rifleman(int hp, int exp, int dmgDone) {
        super(hp, exp, dmgDone);
    }
}

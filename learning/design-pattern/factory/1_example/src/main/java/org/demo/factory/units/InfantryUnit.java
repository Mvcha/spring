package org.demo.factory.units;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class InfantryUnit {
    private int hp;
    private int exp;
    private int dmgDone;

}

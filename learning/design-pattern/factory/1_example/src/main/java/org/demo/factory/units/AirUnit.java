package org.demo.factory.units;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
abstract public class AirUnit {
    private int hp;
    private int exp;
    private int dmgDone;

}

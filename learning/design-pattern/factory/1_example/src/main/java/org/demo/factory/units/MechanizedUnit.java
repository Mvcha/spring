package org.demo.factory.units;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class MechanizedUnit {
    private int hp;
    private int exp;
    private int dmgDone;

}

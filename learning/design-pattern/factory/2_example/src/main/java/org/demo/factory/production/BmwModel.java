package org.demo.factory.production;

public enum BmwModel {
    E60, X7, X6, X5M, X1
}

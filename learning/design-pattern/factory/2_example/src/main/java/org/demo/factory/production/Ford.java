package org.demo.factory.production;

public class Ford extends Car{
    protected Ford(int capacity, Car.FuelType fuelType, int productionYear, String wheelPosition) {
        super(capacity, fuelType, productionYear, wheelPosition);
    }
}

package org.demo.factory.production;

public class CommonwealthFactory implements Factory {
    @Override
    public Bmw buildBmw(BmwModel bmwModel) {
        switch (bmwModel)
        {
            case E60:
                return new Bmw(1999, Car.FuelType.ON,2008, "LEFT");
            case X7:
                return new Bmw(2100, Car.FuelType.PB95,2021, "LEFT");
            case X6:
                return new Bmw(1000,Car.FuelType.PB98, 2014, "LEFT");
            case X5M:
                return new Bmw(1602,Car.FuelType.ON, 2010, "LEFT");
            case X1:
                return new Bmw(2300,Car.FuelType.PB95, 2001, "LEFT");
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
    }
    @Override
    public Ford buildFord(FordModel fordModel) {
        switch (fordModel)
        {
            case CMAX:
                return new Ford(2000,Car.FuelType.PB95, 2008, "LEFT");
            case CAPRI:
                return new Ford(1200,Car.FuelType.ON, 2015, "LEFT");
            case EXPLORER:
                return new Ford(1000,Car.FuelType.PB95, 2010, "LEFT");
            case FIESTA:
                return new Ford(1602,Car.FuelType.PB98, 1999, "LEFT");
            case FOCUS:
                return new Ford(2300,Car.FuelType.ON, 2005, "LEFT");
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
    }
}

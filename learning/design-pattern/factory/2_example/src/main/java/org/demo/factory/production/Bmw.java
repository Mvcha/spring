package org.demo.factory.production;


public class Bmw extends Car {

    protected Bmw(int capacity, Car.FuelType fuelType, int productionYear, String wheelPosition) {
        super(capacity, fuelType, productionYear, wheelPosition);
    }
}

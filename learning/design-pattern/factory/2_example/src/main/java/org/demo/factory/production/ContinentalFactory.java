package org.demo.factory.production;

public class ContinentalFactory implements Factory{
    @Override
    public Bmw buildBmw(BmwModel bmwModel) {
        switch (bmwModel)
        {
            case E60:
                return new Bmw(1999, Car.FuelType.ON,2008, "RIGHT");
            case X7:
                return new Bmw(2100, Car.FuelType.PB95,2021, "RIGHT");
            case X6:
                return new Bmw(1000,Car.FuelType.PB98, 2014, "RIGHT");
            case X5M:
                return new Bmw(1602,Car.FuelType.ON, 2010, "RIGHT");
            case X1:
                return new Bmw(2300,Car.FuelType.PB95, 2001, "RIGHT");
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
    }
    @Override
    public Ford buildFord(FordModel fordModel) {
        switch (fordModel)
        {
            case CMAX:
                return new Ford(2000,Car.FuelType.PB95, 2008, "RIGHT");
            case CAPRI:
                return new Ford(1200,Car.FuelType.ON, 2015, "RIGHT");
            case EXPLORER:
                return new Ford(1000,Car.FuelType.PB95, 2010, "RIGHT");
            case FIESTA:
                return new Ford(1602,Car.FuelType.PB98, 1999, "RIGHT");
            case FOCUS:
                return new Ford(2300,Car.FuelType.ON, 2005, "RIGHT");
            default:
                throw new UnsupportedOperationException("Unknown type");
        }
    }
}

package org.demo.factory.production;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class Car {
    private int capacity;
    private FuelType fuelType;
    private int productionYear;
    private String wheelPosition;


    public enum FuelType{
        ON, PB95, PB98
    }
    public String getWheelPosition() {
        return wheelPosition;
    }
}

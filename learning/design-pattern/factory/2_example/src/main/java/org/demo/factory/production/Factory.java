package org.demo.factory.production;

public interface Factory {
     Bmw buildBmw(BmwModel bmwModel);
     Ford buildFord(FordModel fordModel);
}

package org.demo.factory.production;

public enum FordModel {
    CMAX, CAPRI, EXPLORER, FIESTA, FOCUS
}

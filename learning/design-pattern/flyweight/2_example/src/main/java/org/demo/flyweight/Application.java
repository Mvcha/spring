package org.demo.flyweight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
            ChessPiece blackPawn = new BlackPiece("Black piece",7, "a");
            ChessPiece whitePawn = new WhitePiece("White piece",2,"a");
            ChessPiece blackQueen = new BlackQueen("Black Queen");
            ChessPiece whiteQueen = new WhiteQueen("White Queen");
            //exactly same color object is used
            System.out.println(blackPawn.getColor()==blackQueen.getColor());
            System.out.println(whitePawn.getColor()==whiteQueen.getColor());
    }

}

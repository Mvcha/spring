package org.demo.flyweight;

public class WhiteQueen extends ChessPiece{
    public WhiteQueen(String name) {
        super(name, 1, "d", "white");
    }
}

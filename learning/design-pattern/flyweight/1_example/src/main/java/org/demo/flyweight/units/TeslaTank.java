package org.demo.flyweight.units;

import org.demo.flyweight.UnitStats;
import org.demo.flyweight.repository.UnitStatsRepository;

public class TeslaTank {

    private int x;
    private int y;
    private int hpLeft;
    private UnitStats stats;

    public TeslaTank(int x, int y) {
        this.stats= UnitStatsRepository.getTeslaTankUnitStatsStats();
        this.x = x;
        this.y = y;
        this.hpLeft=stats.getHp();
    }
}

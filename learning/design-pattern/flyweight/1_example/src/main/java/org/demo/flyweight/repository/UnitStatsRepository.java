package org.demo.flyweight.repository;

import org.demo.flyweight.UnitStats;

public class UnitStatsRepository {

    private static UnitStats destroyerUnitStats = new UnitStats("Destroyer", 500,250, 70, 10, 800);
    private static UnitStats riflemanUnitStats = new UnitStats("Rifleman", 25,5, 10,12,20 );
    private static UnitStats teslaTankUnitStats = new UnitStats("TeslaTank", 200, 100, 50,25, 500);

    private UnitStatsRepository () {}

    public static UnitStats getDestroyerStats(){
        return destroyerUnitStats;
    }
    public static UnitStats getTeslaTankUnitStatsStats(){
        return teslaTankUnitStats;
    }
    public static UnitStats getRiflemanUnitStatsStats(){
        return riflemanUnitStats;
    }



}

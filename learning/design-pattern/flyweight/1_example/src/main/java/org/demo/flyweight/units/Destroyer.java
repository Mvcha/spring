package org.demo.flyweight.units;

import lombok.AllArgsConstructor;
import org.demo.flyweight.UnitStats;
import org.demo.flyweight.repository.UnitStatsRepository;

@AllArgsConstructor
public class Destroyer {

    private int x;
    private int y;
    private int hpLeft;
    private UnitStats stats;

    public Destroyer(int x, int y) {
        this.stats= UnitStatsRepository.getDestroyerStats();
        this.x = x;
        this.y = y;
        this.hpLeft=stats.getHp();
    }
}

package org.demo.flyweight;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UnitStats {

    private String name;
    private int hp;
    private int armour;
    private int damageDealt;
    private int speed;
    private int resourceCost;
}

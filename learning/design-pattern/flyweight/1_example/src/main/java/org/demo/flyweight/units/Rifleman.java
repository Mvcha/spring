package org.demo.flyweight.units;

import org.demo.flyweight.UnitStats;
import org.demo.flyweight.repository.UnitStatsRepository;

public class Rifleman {

    private int x;
    private int y;
    private int hpLeft;
    private UnitStats stats;

    public Rifleman(int x, int y) {
        this.stats= UnitStatsRepository.getRiflemanUnitStatsStats();
        this.x = x;
        this.y = y;
        this.hpLeft=stats.getHp();
    }
}

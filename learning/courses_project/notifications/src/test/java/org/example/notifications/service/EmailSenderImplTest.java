package org.example.notifications.service;

import jakarta.mail.MessagingException;
import org.example.notifications.model.dto.EmailDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmailSenderImplTest {


    @Autowired
    private EmailSender emailSender;


    @Test
    void sendEmails() {
    }

    @Test
    void sendEmail() throws MessagingException {
        EmailDto emailDto = EmailDto.builder()
                .to("borys.gdyk@wp.pl")
                .title("Title")
                .content("Content message")
                .build();
        emailSender.sendEmail(emailDto);
    }
}
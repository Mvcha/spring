package org.example.notifications.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@ToString
public class Notification {
    private String courseCode;
    private List<String> emails;
    private String courseName;
    private String courseDescription;
    @JsonFormat(pattern = "yyy-MM-dd'T'HH:mm")
    private LocalDateTime courseStartDate;
    @JsonFormat(pattern = "yyy-MM-dd'T'HH:mm")
    private LocalDateTime courseEndDate;


    public Notification(String courseCode, List<String> emails, String courseName, String courseDescription, LocalDateTime courseStartDate, LocalDateTime courseEndDate) {
        this.courseCode = courseCode;
        this.emails = emails;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.courseStartDate = courseStartDate;
        this.courseEndDate = courseEndDate;
    }
}

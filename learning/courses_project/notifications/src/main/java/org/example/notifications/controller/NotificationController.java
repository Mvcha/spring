package org.example.notifications.controller;

import org.example.notifications.model.Notification;
import org.example.notifications.service.ReceiveService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    private final ReceiveService receiveService;

    public NotificationController(ReceiveService receiveService) {
        this.receiveService = receiveService;
    }

    @GetMapping("/v1/notification")
    public ResponseEntity<Notification> displayNotification() {

        return receiveService.receiveNotification();
    }

}

package org.example.notifications.service;

import org.example.notifications.model.Notification;
import org.springframework.http.ResponseEntity;


public interface ReceiveService {
    ResponseEntity<Notification> receiveNotification();
    void notificationListener(Notification notification);
}

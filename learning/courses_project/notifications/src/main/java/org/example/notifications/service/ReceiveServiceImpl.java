package org.example.notifications.service;

import lombok.extern.slf4j.Slf4j;
import org.example.notifications.model.Notification;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ReceiveServiceImpl implements ReceiveService {

    private final RabbitTemplate rabbitTemplate;
    private final EmailSender emailSender;

    public ReceiveServiceImpl(RabbitTemplate rabbitTemplate, EmailSender emailSender) {
        this.rabbitTemplate = rabbitTemplate;
        this.emailSender = emailSender;
    }



    @Override
    public ResponseEntity<Notification> receiveNotification() {
        Notification notification = rabbitTemplate.receiveAndConvert("enroll_course",
                ParameterizedTypeReference.forType(Notification.class));
        if (notification != null) {
            return ResponseEntity.ok(notification);
        } return ResponseEntity.noContent().build();
    }

    @Override
    @RabbitListener(queues = "enroll_course")
    public void notificationListener(Notification notification) {
        System.out.println("Wyświetlam notification: "+notification.toString());
        emailSender.sendEmails(notification);
        log.info(notification.toString());
    }


}

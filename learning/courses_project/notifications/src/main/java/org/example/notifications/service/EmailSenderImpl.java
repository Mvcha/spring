package org.example.notifications.service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.example.notifications.model.Notification;
import org.example.notifications.model.dto.EmailDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderImpl implements EmailSender {

    private static final Logger log = LoggerFactory.getLogger(EmailSenderImpl.class);
    private final JavaMailSender javaMailSender;

    public EmailSenderImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEmails(Notification notification) {
        String title = "Remember about course: "+notification.getCourseName();
        StringBuilder stringBuilder = createContentMessage(notification);

        notification.getEmails().forEach(email-> {
            try {
                sendMail(email,title, stringBuilder.toString());
            } catch (MessagingException e) {
                log.error("Notification wasn't sent");
                throw new RuntimeException(e);
            }
        });
    }

    private static StringBuilder createContentMessage(Notification notification) {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("The course ");
        stringBuilder.append(notification.getCourseName());
        stringBuilder.append(" starts at ");
        stringBuilder.append(notification.getCourseStartDate().toLocalDate());
        stringBuilder.append(" time: ");
        stringBuilder.append(notification.getCourseStartDate().getHour()).append(":").append(notification.getCourseStartDate().getMinute());
        return stringBuilder;
    }

    public void sendEmail(EmailDto emailDto) throws MessagingException {
        sendMail(emailDto.getTo(), emailDto.getTitle(),emailDto.getContent());
    }

    private void sendMail(String to, String title, String content) throws MessagingException {
        MimeMessage mimeMessage=javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper=new MimeMessageHelper(mimeMessage,true);
        mimeMessage.setFrom("mailtrap@demomailtrap.com");
        mimeMessageHelper.setTo(to);
        mimeMessageHelper.setSubject(title);
        mimeMessageHelper.setText(content, false);
        javaMailSender.send(mimeMessage);
    }
}

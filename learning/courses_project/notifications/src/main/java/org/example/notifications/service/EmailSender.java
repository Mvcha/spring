package org.example.notifications.service;

import jakarta.mail.MessagingException;
import org.example.notifications.model.Notification;
import org.example.notifications.model.dto.EmailDto;

public interface EmailSender {

    void sendEmails(Notification notification);
    void sendEmail(EmailDto emailDto) throws MessagingException;
}

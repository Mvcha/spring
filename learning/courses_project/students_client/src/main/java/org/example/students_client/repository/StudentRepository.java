package org.example.students_client.repository;

import jakarta.validation.constraints.Email;
import org.example.students_client.entity.Status;
import org.example.students_client.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Consumer;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Boolean existsByEmail(String email);
    List<Student> findStudentByStatus(Status status);

    List<Student> findAllByEmailIn(List<String> emails);
}

package org.example.students_client.service;

import org.example.students_client.entity.Status;
import org.example.students_client.entity.Student;

import java.util.List;

public interface StudentService {


    void saveStudent(Student student);

    void deleteStudent(Long id);

    Student findStudentById(Long studentId);

    Student putStudent(Long id, Student student);

    Student patchStudent(Long id, Student student);

    boolean dbIsEmpty();

    List<Student> findAllStudents();

    List<Student> findStudentsByStatus(Status status);


    List<Student> getStudentsByEmail(List<String> emails);
}

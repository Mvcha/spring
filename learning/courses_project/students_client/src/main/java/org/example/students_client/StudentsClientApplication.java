package org.example.students_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentsClientApplication.class, args);
    }

}

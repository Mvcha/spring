package org.example.students_client.controller;

import jakarta.validation.Valid;
import org.example.students_client.entity.Status;
import org.example.students_client.entity.Student;
import org.example.students_client.service.StudentServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {


    private StudentServiceImpl studentService;

    public StudentController( StudentServiceImpl studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/fill")
    public String createStudents() {
        if (studentService.dbIsEmpty() == true) {

            studentService.saveStudent(new Student("jan@gmail.com", "Jan", "Nowak", Status.INACTIVE));
            studentService.saveStudent(new Student("borys.gdyk@wp.pl", "Borys", "Owen",Status.ACTIVE));
            studentService.saveStudent(new Student("zofia@gmail.com", "Zofia", "Nowak", Status.ACTIVE));
            studentService.saveStudent(new Student("john@gmail.com", "John", "Boris",Status.ACTIVE));
            studentService.saveStudent(new Student("peter@gmail.com", "Peter", "Borovski",Status.ACTIVE));
            return "Added data";
        }
        return "DB was filled";
    }

    @PostMapping("/add")
    public void  addStudent(@Valid @RequestBody Student student) {
        studentService.saveStudent(student);
    }

    @GetMapping
    public List<Student> getStudents(@RequestParam(required = false) Status status){
        if(status==null)
        {
            return studentService.findAllStudents();
        }
        else {
            return studentService.findStudentsByStatus(status);
        }
    }

    @GetMapping("/{studentId}")
    public Student getStudent(@PathVariable Long studentId){
        return studentService.findStudentById(studentId);
    }


    @DeleteMapping("/{studentId}")
    public void deleteStudent(@PathVariable Long studentId){
        studentService.deleteStudent(studentId);
    }
    @PutMapping("/{studentId}")
    public void putStudent(@PathVariable Long studentId, @Valid @RequestBody Student student){
        studentService.putStudent(studentId, student);
    }

    @PostMapping("/emails")
    public List<Student> getStudentsByEmail(@RequestBody List<String> emails)
    {
        return studentService.getStudentsByEmail(emails);
    }

}

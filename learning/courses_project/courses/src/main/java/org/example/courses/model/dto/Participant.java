package org.example.courses.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class Participant {

    private String email;
    private LocalDateTime dateTime;

    public Participant(String email, LocalDateTime dateTime) {
        this.email = email;
        this.dateTime = dateTime;
    }
}

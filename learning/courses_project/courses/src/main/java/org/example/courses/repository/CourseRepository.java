package org.example.courses.repository;

import org.example.courses.model.Course;
import org.example.courses.model.Status;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {

    List<Course> findCourseByStatus(Status status);
}

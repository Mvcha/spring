package org.example.courses.service;

import org.example.courses.model.dto.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "student-service",path = "/students")
public interface StudentServiceClient {

    @GetMapping("/{studentId}")
    Student getStudentById(@PathVariable Long studentId);

    @PostMapping("/emails")
    List<Student> getStudentsListByEmails(@RequestBody List<String> emails);
}

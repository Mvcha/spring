package org.example.courses.controller;

import jakarta.validation.Valid;
import org.example.courses.model.Course;
import org.example.courses.model.Status;
import org.example.courses.model.dto.Participant;
import org.example.courses.model.dto.Student;
import org.example.courses.service.CourseService;
import org.example.courses.service.PublisherService;
import org.example.courses.service.StudentServiceClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController {


    private final CourseService courseService;
    private final PublisherService publisherService;

    public CourseController(CourseService courseService, PublisherService publisherService) {
        this.courseService = courseService;
        this.publisherService = publisherService;
    }

    @GetMapping
    public List<Course> getCourses(@RequestParam(required = false) Status status) {
        return courseService.getCourses(status);
    }

    @DeleteMapping
    public void deleteCourse(@RequestParam String code) {
        courseService.deleteCourse(code);
    }

    @PostMapping
    public Course addCourse(@Valid @RequestBody Course course) {
        return courseService.addCourse(course);
    }

    @GetMapping("/{code}")
    public Course getCourse(@PathVariable String code) {
        return courseService.getCourse(code);
    }

    @PutMapping
    public Course putCourse(@Valid @RequestBody Course course, String code) {
        return courseService.putCourse(course, code);
    }

    @PostMapping("/{code}/student/{idStudent}")
    public ResponseEntity<?> addStudentToCourse(@PathVariable String code,
                                             @PathVariable Long idStudent) {

        courseService.addStudentToCourse(code, idStudent);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/{code}/participantList")
    public List<Participant> getParticipantsListInCourse(@PathVariable String code) {
        return courseService.getParticipantList(code);
    }
    @GetMapping("/{code}/participantsDetails")
    public List<Student> getPersonalInformationAboutParticipants(@PathVariable String code) {
        return courseService.getParticipantPersonalInformation(code);
    }

    @PostMapping("/{code}/finish-enroll")
    public ResponseEntity<?> finishEnrollToTheCourse(@PathVariable String code){
        courseService.finishEnroll(code);
        return ResponseEntity.ok().build();
    }
}

package org.example.courses.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import org.bson.json.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public class CourseExceptionHandler {

    @ExceptionHandler(CourseException.class)
    public ResponseEntity<ErrorInfo> courseErrorResponseEntity(CourseException courseException) {
        switch (courseException.getCourseError()) {
            case COURSE_NOT_FOUND:
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorInfo(courseException.getCourseError().getMessage()));

            case COURSE_ERROR, COURSE_DATA_ERROR, COURSE_IS_NOT_ACTIVE, STUDENT_IS_NOT_ACTIVE, COURSE_IS_INACTIVE:
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorInfo(courseException.getCourseError().getMessage()));
            case COURSE_PARTICIPANTS_ERROR, COURSE_FULL_PARTICIPANTS_ERROR, COURSE_CONFLICT:
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorInfo(courseException.getCourseError().getMessage()));
            default:
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorInfo("Other error"));

        }
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<?> handleFeignException(FeignException feignException) throws JsonProcessingException {

       // return ResponseEntity.status(feignException.status()).body(new JsonObject(feignException.contentUTF8()));


        return ResponseEntity.status(feignException.status()).body(new ObjectMapper().readValue(feignException.contentUTF8(), HashMap.class));
    }
}

package org.example.courses.service;

import org.example.courses.exception.CourseError;
import org.example.courses.exception.CourseException;
import org.example.courses.model.Course;
import org.example.courses.model.Status;
import org.example.courses.model.dto.Participant;
import org.example.courses.model.dto.Student;
import org.example.courses.repository.CourseRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService{
    private final CourseRepository courseRepository;
    private final StudentServiceClient studentServiceClient;
    private final PublisherService publisherService;

    public CourseServiceImpl(CourseRepository courseRepository,
                             StudentServiceClient studentServiceClient,
                             PublisherService publisherService) {
        this.courseRepository = courseRepository;
        this.studentServiceClient = studentServiceClient;
        this.publisherService = publisherService;
    }

    @Override
    public List<Course> getCourses(Status status) {
        if(status!=null){
            return getCoursesByStatus(status);
        }
        return courseRepository.findAll();
    }

    @Override
    public List<Course> getCoursesByStatus(Status status) {
        return courseRepository.findCourseByStatus(status);
    }

    @Override
    public Course getCourse(String code) {

        return courseRepository.findById(code)
                .orElseThrow(()-> new CourseException(CourseError.COURSE_NOT_FOUND));
    }

    @Override
    public Course addCourse(Course course) {
        course.validateCourse();
        return courseRepository.save(course);
    }

    @Override
    public void deleteCourse(String code) {
        courseRepository.delete(getCourse(code));
    }

    @Override
    public Course putCourse(Course course, String code) {
        return courseRepository.findById(code)
                .map(dbCourse -> {
                    course.validateCourse();
                    dbCourse.setCode(course.getCode());
                    dbCourse.setName(course.getName());
                    dbCourse.setDescription(course.getDescription());
                    dbCourse.setStartDate(course.getStartDate());
                    dbCourse.setEndDate(course.getEndDate());
                    dbCourse.setParticipantsLimit(course.getParticipantsLimit());
                    dbCourse.setParticipantsNumber(course.getParticipantsNumber());
                    dbCourse.setStatus(course.getStatus());
                    dbCourse.setParticipantList(course.getParticipantList());
                    return courseRepository.save(course);
                }).orElseThrow(()->new CourseException(CourseError.COURSE_NOT_FOUND));

    }

    @Override
    public void addStudentToCourse(String code, Long idStudent) {
        Student student=studentServiceClient.getStudentById(idStudent);
        Course course=getCourse(code);

        if(!course.getStatus().equals(Status.ACTIVE)){
            throw new CourseException(CourseError.COURSE_IS_NOT_ACTIVE);
        }
        if(!student.getStatus().equals(Student.Status.ACTIVE)){
            throw new CourseException(CourseError.STUDENT_IS_NOT_ACTIVE);
        }
        Participant participant=new Participant(student.getEmail(), LocalDateTime.now() );
        course.addParticipantToList(participant);
        course.setParticipantsNumber(course.getParticipantsNumber()+1);
        if(course.getParticipantsNumber()== course.getParticipantsLimit()){
            course.setStatus(Status.FULL);
        }
        courseRepository.save(course);
    }

    @Override
    public List<Participant> getParticipantList(String code) {
        return getCourse(code).getParticipantList();
    }

    @Override
    public List<Student> getParticipantPersonalInformation(String code) {


        return studentServiceClient.getStudentsListByEmails(getEmailList(code))
                .stream()
                .map(student -> new Student(
                        student.getFirstName(),
                        student.getLastName(),
                        student.getEmail()))
                .collect(Collectors.toList());

    }

    @Override
    public void finishEnroll(String code) {
        Course course=getCourse(code);

        if(course.getStatus().equals(Status.INACTIVE))throw new CourseException(CourseError.COURSE_IS_INACTIVE);
        course.setStatus(Status.INACTIVE);
        courseRepository.save(course);
        publisherService.publicNotificationAboutEnrollingInTheCourse(course,  getEmailList(code));
    }


    private List<String> getEmailList(String code){
        return  getCourse(code).getParticipantList().stream()
                .map(participant -> participant.getEmail())
                .collect(Collectors.toList());
    }
}

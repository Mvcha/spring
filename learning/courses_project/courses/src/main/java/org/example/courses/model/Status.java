package org.example.courses.model;

public enum Status {
    INACTIVE, ACTIVE, FULL
}

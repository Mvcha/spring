package org.example.courses.model;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.example.courses.exception.CourseError;
import org.example.courses.exception.CourseException;
import org.example.courses.model.dto.Participant;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;

@Document
@Getter
@Setter
@ToString
public class Course {

    @Id
    private String code;
    @NotBlank
    private String name;
    private String description;
    @NotNull
    @Future
    private LocalDateTime startDate;
    @NotNull
    @Future
    private LocalDateTime endDate;
    @Min(0)
    private Long participantsLimit;
    @NotNull
    @Min(0)
    private Long participantsNumber;
    @NotNull
    private Status status;

    private List<Participant> participantList = new ArrayList<>();


    public void validateCourse()
    {
        validateDate();
        validateParticipant();
        validateFullStatus();
    }

    private void validateDate(){

        if(startDate.isAfter(endDate)) throw new CourseException(CourseError.COURSE_DATA_ERROR);

    }
    private void validateParticipant(){
        if (participantsNumber>participantsLimit)  throw new CourseException(CourseError.COURSE_PARTICIPANTS_ERROR);
    }
    private void validateFullStatus()
    {
        if(status==Status.FULL && participantsNumber<participantsLimit) throw new CourseException(CourseError.COURSE_FULL_PARTICIPANTS_ERROR);
    }
    private void validateIfParticipantListContainsThisParticipant(Participant participant) {

        participantList.forEach(participant1 -> {
            if (participant1.getEmail().equals(participant.getEmail())) throw new CourseException(CourseError.COURSE_CONFLICT);
        });

    }
    public void addParticipantToList(Participant participant){
        validateIfParticipantListContainsThisParticipant(participant);
        participantList.add(participant);
    }

}

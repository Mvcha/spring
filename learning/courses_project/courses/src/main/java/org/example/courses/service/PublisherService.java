package org.example.courses.service;

import org.example.courses.model.Course;
import org.example.courses.model.dto.Notification;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class PublisherService {

    private final RabbitTemplate rabbitTemplate;


    public PublisherService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void publicNotificationAboutEnrollingInTheCourse(Course course, List<String> emails){
        Notification notification=new Notification(
                course.getCode(),
                emails,
                course.getName(),
                course.getDescription(),
                course.getStartDate(),
                course.getEndDate());
        rabbitTemplate.convertAndSend("enroll_course", notification);
    }
}

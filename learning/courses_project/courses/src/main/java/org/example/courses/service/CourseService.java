package org.example.courses.service;

import org.example.courses.model.Course;
import org.example.courses.model.Status;
import org.example.courses.model.dto.Participant;
import org.example.courses.model.dto.Student;


import java.util.List;


public interface CourseService {
    List<Course> getCourses(Status status);
    List<Course> getCoursesByStatus(Status status);
    Course getCourse(String code);
    Course addCourse(Course course);
    void deleteCourse(String code);
    Course putCourse(Course course, String code);
    void addStudentToCourse(String code, Long idStudent);
    List<Participant> getParticipantList(String code);
    List<Student> getParticipantPersonalInformation(String code);

    void finishEnroll(String code);
}

package org.example.courses.exception;

public enum CourseError {
    COURSE_NOT_FOUND("Course doesn't exist"),
    COURSE_DATA_ERROR("Start date is higher than end date"),
    COURSE_PARTICIPANTS_ERROR("Amount of participants is higher than limit"),
    COURSE_FULL_PARTICIPANTS_ERROR("Course has full status, but user limit has not been reached"),
    COURSE_ERROR("Other error with course"),
    COURSE_CONFLICT("Student is already enrolled in this course!"),
    COURSE_IS_NOT_ACTIVE("Course isn't active"),
    COURSE_IS_INACTIVE("Course is inactive"),
    STUDENT_IS_NOT_ACTIVE("Student inactive");

    private String message;

    CourseError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

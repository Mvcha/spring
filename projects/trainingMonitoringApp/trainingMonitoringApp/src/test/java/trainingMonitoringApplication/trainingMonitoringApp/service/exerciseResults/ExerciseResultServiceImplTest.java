package trainingMonitoringApplication.trainingMonitoringApp.service.exerciseResults;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.MockitoAnnotations;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.ExerciseResult;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.repository.exerciseResults.ExerciseResultRepository;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ExerciseResultServiceImplTest {


    private ObjectMapper objectMapper = new ObjectMapper();
    @InjectMocks
    ExerciseResultRepository exerciseResultRepository = Mockito.mock(ExerciseResultRepository.class);
    @Mock
    ExerciseService exerciseService = Mockito.mock(ExerciseService.class);

    @InjectMocks
    private ExerciseResultServiceImpl exerciseResultServiceImpl = new ExerciseResultServiceImpl(objectMapper, exerciseResultRepository, exerciseService);

    @Mock
    ExerciseResultService exerciseResultService=Mockito.mock(ExerciseResultService.class);



    private User user;
    private ExercisesCategory exercisesCategory;
    private Exercise exercise;
    private Exercise exercise2;
    private ExerciseResult exerciseResult1;
    private ExerciseResult exerciseResult2;
    private ExerciseResult exerciseResult3;


    @BeforeEach
    public void setUp() {
        user = new User("test123", "test", true);
        exercisesCategory = new ExercisesCategory(7, "Category");
        exercise = new Exercise(1001,"TestExercise", exercisesCategory);
        exercise2 = new Exercise(1002, "TestExercise2", exercisesCategory);
        exerciseResult1 = new ExerciseResult(exercise, "10.0 ,10.0", "12, 12", user, "2024-03-28");
        exerciseResult2 = new ExerciseResult(exercise, "20.0 ,20.0", "10, 10", user, "2024-02-28");
        exerciseResult3 = new ExerciseResult(exercise, "20.0 ,20.0", "10, 10", user, "2024-04-03");

    }


    @Test
    void convertListToJson() throws JsonProcessingException {
        List list = Arrays.asList("FirstElement", "Second", "Third");

        String json = exerciseResultServiceImpl.convertListToJson(list);
        assertNotNull(json, "Not null");
        assertTrue(json.contains("FirstElement"), "k");
        assertTrue(json.contains("Second"));
        assertTrue(json.contains("Third"));
        assertFalse(json.contains("Fifth"));
    }

    @Test
    void convertJsonToNumberList() throws JsonProcessingException {

        List list = Arrays.asList(11, 22, 13);
        String json = "[11,22,13]";

        Assertions.assertEquals(json, exerciseResultServiceImpl.convertListToJson(list));
    }

    @Test
    void deleteNullsFromExerciseResultMapRemoveEntireEmptyExercise() {
        Map<Integer, ExerciseResultDto> map = new HashMap<>();
        map.put(1, new ExerciseResultDto(1, Arrays.asList(1.0, 1.0, 1.0, 1.0), Arrays.asList(2, 2, 2, 2), null));


        map.put(2, new ExerciseResultDto(1, Arrays.asList(1.0, 1.0, 1.0, 1.0), Arrays.asList(2, 2, 2, 2), null));
        map.put(3, new ExerciseResultDto(1, Arrays.asList(1.0, 1.0, 1.0, 1.0), new ArrayList<>(), null));
        map.put(4, new ExerciseResultDto(1, new ArrayList<>(), Arrays.asList(2, 2, 2, 2), null));

        Assertions.assertEquals(2, exerciseResultServiceImpl.deleteNullsFromExerciseResultMap(map).size());
    }

    @Test
    void deleteNullsFromExerciseResultMapRemoveSeriesWithoutWeightOrRepetitions() {
        Map<Integer, ExerciseResultDto> map = new HashMap<>();
        ExerciseResultDto exerciseResultDto1 = new ExerciseResultDto(1, new ArrayList<>(Arrays.asList(1.0, 1.0)), new ArrayList<>(Arrays.asList(2, 2)), null);
        exerciseResultDto1.setWeight(new ArrayList<>(Arrays.asList(1.0, 1.0, null, 1.0)));
        exerciseResultDto1.setRepetitions(new ArrayList<>(Arrays.asList(2, 2, 2, null)));
        map.put(1, exerciseResultDto1);

        ExerciseResultDto expected = new ExerciseResultDto(1, new ArrayList<>(Arrays.asList(1.0, 1.0)), new ArrayList<>(Arrays.asList(2, 2)), null);
        ExerciseResultDto actual = exerciseResultServiceImpl.deleteNullsFromExerciseResultMap(map).get(1);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void showExerciseResults() {

        Mockito.when(exerciseResultRepository.findByUser(user)).thenReturn(Arrays.asList(exerciseResult1, exerciseResult2));

        LinkedHashMap<Exercise, List<ExerciseResult>> showResults = exerciseResultServiceImpl.showExerciseResults(user);

        Assertions.assertEquals(2, showResults.size());

    }

    @Test
    void showResultsForExercise() {
        Mockito.when(exerciseResultRepository.findByUserAndExerciseOrderByDate(user, exercise)).thenReturn(Arrays.asList(exerciseResult1, exerciseResult2, exerciseResult3));

        List<ExerciseResult> listOfExercisesResults = exerciseResultRepository.findByUserAndExerciseOrderByDate(user, exercise);

        assertEquals(3, listOfExercisesResults.size());
    }

    @Test
    void save() {
        MockitoAnnotations.openMocks(this);
        System.out.println(exerciseResult1);
        Mockito.verify(exerciseResultService).save(exerciseResult1);
    }

    @Test
    void convertListOfObjectsToJson() {
    }

    @Test
    void exerciseResultDtoToExerciseResultChartDto() {
    }

    @Test
    void testSave() {
    }


}
package trainingMonitoringApplication.trainingMonitoringApp.service.calculators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import trainingMonitoringApplication.trainingMonitoringApp.model.OneRmData;

import java.util.ArrayList;
import java.util.List;



class OneRmServiceTest {

    private OneRmService oneRmService;
    @BeforeEach
    void setUp() {
    oneRmService=new OneRmService();
    }

    @Test
    void calculateOneRMIllegalArgument() {
        double lift=11;
        int repetitions=0;

        Assertions.assertThrows(IllegalArgumentException.class, ()->oneRmService.calculateOneRM(lift,repetitions));
    }
    @Test
    void calculateOneRM() {
        double lift=100;
        int repetitions=10;

        Assertions.assertEquals(133.3,oneRmService.calculateOneRM(lift,repetitions));
    }

    @Test
    void rmValues() {
        double oneRm=133.3;
        List<OneRmData> rmResults =new ArrayList<>();
        rmResults.add(new OneRmData(100, 133.3,1));
        rmResults.add(new OneRmData(95, 126.6,2));
        rmResults.add(new OneRmData(90, 120.0,4));
        rmResults.add(new OneRmData(85, 113.3,6));
        rmResults.add(new OneRmData(80, 106.6,8));
        rmResults.add(new OneRmData(75, 100.0,10));
        rmResults.add(new OneRmData(70, 93.3,12));

        Assertions.assertEquals(rmResults, oneRmService.rmValues(oneRm));
    }
}
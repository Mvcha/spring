package trainingMonitoringApplication.trainingMonitoringApp.service.calculators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MassCalculatorServiceTest {

    private MassCalculatorService massCalculatorService;

    @BeforeAll
    public void setup() {
        massCalculatorService = new MassCalculatorService();
    }

    @Test
    void bmiFactorIllegalArgument() {
        BigDecimal weight= new BigDecimal(0);
        BigDecimal height=new BigDecimal(0);

        Assertions.assertThrows(IllegalArgumentException.class, ()-> massCalculatorService.bmiFactor(weight,height));
    }

    @Test
    void bmiFactorCorrect() {
        BigDecimal weight= new BigDecimal(80);
        BigDecimal height=new BigDecimal(180);

        Assertions.assertEquals(new BigDecimal(24.69).setScale(2, RoundingMode.DOWN), massCalculatorService.bmiFactor(weight, height));
    }
    @ParameterizedTest
    @CsvSource({
            "15.00, underweight II",
            "15.99, underweight II",
            "18.49, underweight",
            "24.99, OK",
            "29.9, overweight",
            "30, obesity",
    })
    void bmiResult(double value, String result) {

        assertEquals(result, massCalculatorService.bmiResult(value));
    }

    @Test
    void rfmFactorIllegalArgument() {
        BigDecimal waist=new BigDecimal(0);
        BigDecimal height=new BigDecimal(0);
        String gender ="Male";

        Assertions.assertThrows(IllegalArgumentException.class, ()-> massCalculatorService.rfmFactor(gender, height, waist));
    }
    @ParameterizedTest
    @CsvSource({
            "Female, 27",
            "Male, 15"
    })
    void rfmFactor(String gender, double result) {
        BigDecimal waist=new BigDecimal(70);
        BigDecimal height=new BigDecimal(170);
        Assertions.assertEquals(result, massCalculatorService.rfmFactor(gender, height, waist));
    }

    @ParameterizedTest
    @CsvSource({
            "6, underweight",
            "7, OK",
            "19, OK",
            "20, overweight",
            "25, overweight",
            "26, obesity",
    })
    void rfmResultForMaleOverEighteen(int rfmFactor, String result) {

        assertEquals(result, massCalculatorService.rfmResult(rfmFactor,"Male",19));
    }

    @ParameterizedTest
    @CsvSource({
            "11, underweight",
            "12, OK",
            "20, OK",
            "21, overweight",
            "25, overweight",
            "28, obesity",
    })
    void rfmResultForMaleBelowEighteen(int rfmFactor, String result) {
        assertEquals(result, massCalculatorService.rfmResult(rfmFactor,"Male",17));
    }

    @ParameterizedTest
    @CsvSource({
            "20, underweight",
            "21, OK",
            "32, OK",
            "33, overweight",
            "38, overweight",
            "40, obesity",

    })
    void rfmResultForFemaleOverEighteen(int rfmFactor, String result) {
        assertEquals(result, massCalculatorService.rfmResult(rfmFactor,"Female",19));

    }
    @ParameterizedTest
    @CsvSource({
            "15, underweight",
            "16, OK",
            "29, OK",
            "30, overweight",
            "34, overweight",
            "35, obesity",
    })
    void rfmResultForFemaleBelowEighteen(int rfmFactor, String result) {
        assertEquals(result, massCalculatorService.rfmResult(rfmFactor,"Female",17));
    }
}
package trainingMonitoringApplication.trainingMonitoringApp.controller.user.plan;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import trainingMonitoringApplication.trainingMonitoringApp.dto.PlanDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.PlanDetails;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;
import trainingMonitoringApplication.trainingMonitoringApp.service.plan.PlanDetailsService;
import trainingMonitoringApplication.trainingMonitoringApp.service.plan.PlanService;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

import java.security.Principal;

import java.util.*;

@Controller
@RequestMapping("/user/plan")
public class TrainingPlanController {

    private final PlanService planService;
    private final PlanDetailsService planDetailsService;
    private final UserService userService;
    private final ExerciseService exerciseService;
   // private final OwnExerciseService ownExerciseService;

    public TrainingPlanController(PlanService planService,
                                  PlanDetailsService planDetailsService,
                                  UserService userService,
                                  ExerciseService exerciseService
                                  ) {
        this.planService = planService;
        this.planDetailsService = planDetailsService;
        this.userService = userService;
        this.exerciseService = exerciseService;
    }

    @GetMapping("/")
    public String showMainPlanPage() {
        return "user/trainingPlan/trainingPageIndex";
    }

    @GetMapping("/add")
    public String newPlanForm() {
        return "user/trainingPlan/addNewPlanForm";
    }
    @GetMapping("/edit/{id}")
    public String editPlanForm(@PathVariable("id") int id,
                               Model model,
                               Principal principal) {
        User user=userService.findUserByUsername(principal.getName());
        PlanDto plan = planService.planDtoByPlanId(id,user);
        model.addAttribute("plan", plan);

        return "user/trainingPlan/editPlanForm";
    }

    @PostMapping("/addProcessing")
    public String addNewPlan(@ModelAttribute("plan") Plan plan,
                             Principal principal) {
        plan.setUser(userService.findUserByUsername(principal.getName()));
        planService.save(plan);

        return "redirect:/user/plan/list";
    }


    @GetMapping("/list")
    public String showPlansList(Principal principal,
                                Model model) {
        List<PlanDto> plans =planService.findAllPlansByUser(userService.findUserByUsername(principal.getName()));
        model.addAttribute("trainingPlans", plans);
        return "user/trainingPlan/listOfTrainingPlans";
    }
    @GetMapping("/prepared/list")
    public String showPlansListReadyToGo(Principal principal,
                                Model model) {

        List<PlanDto> plans =planService.findAllPlansByUser(userService.findUserByUsername(principal.getName()));
        model.addAttribute("trainingPlans", plans);
        return "user/trainingPlan/doWorkoutPlanList";
    }

    @GetMapping("/manage/{id}")
    public String manageTraining(@PathVariable("id") int id,
                                 Model model, Principal principal) {
        User user=userService.findUserByUsername(principal.getName());
        Plan plan = planService.findByPlanIdAndUser(id, user);
        List<PlanDetails> listOfPlanDetails = planDetailsService.findAllByPlan(plan);
        model.addAttribute("trainingPlans", plan);
        model.addAttribute("listOfPlanDetails", listOfPlanDetails);
        model.addAttribute("listOfAddedExercises", planDetailsService.listOfExercises(listOfPlanDetails));
        model.addAttribute("chestExercises", exerciseService.findExerciseByCategoryName("Chest", user.getUserId()));
        model.addAttribute("backExercises", exerciseService.findExerciseByCategoryName("Back", user.getUserId()));
        model.addAttribute("gluteExercises", exerciseService.findExerciseByCategoryName("Glute", user.getUserId()));
        model.addAttribute("abdominalsExercises", exerciseService.findExerciseByCategoryName("Abdominals", user.getUserId()));
        model.addAttribute("legsExercises", exerciseService.findExerciseByCategoryName("Legs", user.getUserId()));
        model.addAttribute("shouldersExercises", exerciseService.findExerciseByCategoryName("Shoulders", user.getUserId()));
        return "user/trainingPlan/planManagement";
    }

    @GetMapping("/delete/{id}")
    public String manageTraining(@PathVariable("id") int id) {
        planService.deleteByPlanId(id);

        return "redirect:/user/plan/list";
    }

    @RequestMapping("/add/exercise")
    public String values(@RequestParam(value = "checkedCheckboxes", required = false) List<String> checkboxes,
                         @RequestParam("itemId") int id, Principal principal) {

        User user=userService.findUserByUsername(principal.getName());
        List<PlanDetails> detailsList = planDetailsService.findAllByPlan(planService.findByPlanIdAndUser(id,user));
        planDetailsService.removeUnchecked(detailsList, checkboxes);
        planDetailsService.save(checkboxes, planService.findByPlanIdAndUser(id,user));

        return "redirect:/user/plan/manage/" + id;
    }



}

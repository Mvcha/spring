package trainingMonitoringApplication.trainingMonitoringApp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;
import trainingMonitoringApplication.trainingMonitoringApp.repository.user.UserDetailsRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

    private UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public UserDetails findByEmail(String email) {
        return userDetailsRepository.findByEmail(email);
    }
}

package trainingMonitoringApplication.trainingMonitoringApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TrainingMonitoringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingMonitoringAppApplication.class, args);
	}

	}

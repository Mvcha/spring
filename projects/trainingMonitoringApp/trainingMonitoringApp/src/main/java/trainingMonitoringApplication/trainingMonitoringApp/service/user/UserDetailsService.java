package trainingMonitoringApplication.trainingMonitoringApp.service.user;

import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

public interface UserDetailsService {

    UserDetails findByEmail(String email);
}

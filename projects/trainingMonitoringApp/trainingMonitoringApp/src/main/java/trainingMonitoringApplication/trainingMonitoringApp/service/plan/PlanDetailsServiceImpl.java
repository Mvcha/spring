package trainingMonitoringApplication.trainingMonitoringApp.service.plan;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.PlanDetails;
import trainingMonitoringApplication.trainingMonitoringApp.repository.plan.PlanDetailsRepository;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;


import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlanDetailsServiceImpl implements PlanDetailsService {

    private final PlanDetailsRepository planDetailsRepository;
    private final ExerciseService exerciseService;


    public PlanDetailsServiceImpl(PlanDetailsRepository planDetailsRepository,
                                  ExerciseService exerciseService) {
        this.planDetailsRepository = planDetailsRepository;
        this.exerciseService = exerciseService;
    }

    @Override
    public void save(PlanDetails planDetails) {

        planDetailsRepository.save(planDetails);
    }

    @Override
    public void save(List<String> exercisesList, Plan plan) {

        if (exercisesList != null) {
            for (String exercise : exercisesList) {
                if (findByPlanAndExercise(plan, exerciseService.findByName(exercise)) == null) {
                    PlanDetails planDetails = new PlanDetails(plan, exerciseService.findByName(exercise),false);
                    planDetailsRepository.save(planDetails);

                }
            }
        }
    }

    @Override
    @Transactional
    public void removeUnchecked(List<PlanDetails> detailsList, List<String> checkboxes) {

        if (checkboxes == null) {
            for (PlanDetails detail : detailsList) {
                planDetailsRepository.delete(detail);
            }
        } else {
            for (PlanDetails detail : detailsList) {
                if (!checkboxes.contains(detail.getExercise().getName())) {
                    planDetailsRepository.delete(detail);
                }
            }
        }
    }

    @Override
    public List<PlanDetails> findByPlan(Plan plan) {
        return planDetailsRepository.findByPlan(plan);
    }

    @Override
    public Boolean isExist(Plan plan) {

        if (findByPlan(plan).isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public List<PlanDetails> findAllByPlan(Plan plan) {
        return planDetailsRepository.findAllByPlan(plan);
    }

    @Override
    public List<String> listOfExercises(List<PlanDetails> listOfPlanDetails) {
        return listOfPlanDetails.stream()
                .map(name -> name.getExercise().getName())
                .collect(Collectors.toList());
    }

    @Override
    public PlanDetails findByPlanAndExercise(Plan plan, Exercise exercise) {
        return planDetailsRepository.findByPlanAndExercise(plan, exercise);
    }

    @Override
    public Map<String, List<ExerciseDto>> groupExerciseByCategoryFromListOfExercises(List<String> listOfExercises) {
        List <ExerciseDto> exercisesDto=new ArrayList<>();
        Map<String, List<ExerciseDto>> mapCategoryAndExercises=new HashMap<>();

        for (String exercise : listOfExercises) {
            exercisesDto.add(new ExerciseDto(exerciseService.findByName(exercise).getExerciseId(),
                    exercise,
                    exerciseService.findByName(exercise).getExercisesCategory().getName()));
        }

        return exercisesDto.stream()
                .collect(Collectors.groupingBy(ExerciseDto::getCategoryName));


/*        maps.forEach((category, exercises) -> {
            System.out.println("Category: " + category);
            for (ExerciseDto exercise : exercises) {
                System.out.println("\tExercise name: " + exercise.getName());
            }
        });*/

     /*exercisesDto.stream()
                .filter(exerciseDto -> exerciseDto.getCategoryName().equals(categoryName))
            .map(exerciseDto -> new ExerciseDto(exerciseDto.getName(), exerciseDto.getCategoryName()))
            .collect(Collectors.toList());*/
    }
}

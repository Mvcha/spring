package trainingMonitoringApplication.trainingMonitoringApp.service.exerciseResults;

import com.fasterxml.jackson.core.JsonProcessingException;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultChartDto;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.ExerciseResult;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface ExerciseResultService {

    void save(ExerciseResult exerciseResult);
    void save(User user, Map<Integer, ExerciseResultDto> map) throws JsonProcessingException;
    String convertListToJson(List<? extends Number> list) throws JsonProcessingException;
    String convertListOfObjectsToJson(List<ExerciseResultChartDto> exerciseResultChartDto) throws JsonProcessingException;
    List<ExerciseResultChartDto> exerciseResultDtoToExerciseResultChartDto(List<ExerciseResultDto> exerciseResultDto);
    Map<Integer, ExerciseResultDto> deleteNullsFromExerciseResultMap(Map<Integer, ExerciseResultDto> map);
    LinkedHashMap<Exercise, List<ExerciseResult>> showExerciseResults(User user);
    List<ExerciseResultDto> showResultsForExercise(User user, Exercise exercise);
    List<Number> convertJsonToNumberList(String json) throws JsonProcessingException;
}

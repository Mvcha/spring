package trainingMonitoringApplication.trainingMonitoringApp.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
    User findById(int id);
    List<User> findAll();


    User findUserByUserDetails(UserDetails userDetails);
   /* @Query("SELECT u FROM User u inner join UserDetails us on us.userDetailId=u.userDetails where us.email=? ")
    User findUserByEmail(String email);*/
}

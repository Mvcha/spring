package trainingMonitoringApplication.trainingMonitoringApp.controller.admin;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import trainingMonitoringApplication.trainingMonitoringApp.dto.UsersListDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserDetailsService;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

import java.util.List;

@Controller
@RequestMapping("/admin/users")
public class ManagementController {

    private UserService userService;
    private UserDetailsService userDetailsService;

    public ManagementController(UserService userService, UserDetailsService userDetailsService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping("/list")
    public String getUsersList(Model model) {

        List<UsersListDto> usersListDto = userService.findAllUsersWithDetails();
        model.addAttribute("usersList", usersListDto);
        return "admin/usersList";
    }

    @GetMapping("/user/details")
    public String displayUserDetails(@RequestParam("userId") int userId,
                                     Model model) {

        User user = userService.findUserById(userId);
        model.addAttribute("user", user);

        return "admin/userDetails";
    }

    @GetMapping("/edit/form")
    public String showFormToEditUser(@RequestParam("userId") int userId,
                                     @Valid @ModelAttribute("user") User user,
                                     BindingResult bindingUserResult,
                                     @Valid @ModelAttribute("userDetails") UserDetails userDetails,
                                     BindingResult bindingUserDetailsResult,
                                     Model model) {
        User tempUser = userService.findUserById(userId);
        UserDetails tempUserDetails = tempUser.getUserDetails();
        model.addAttribute("user", tempUser);
        model.addAttribute("userDetails", tempUserDetails);

        return "admin/userEditForm";
    }

    @GetMapping("/updatePrivileges")
    public String changePrivileges(@RequestParam("userId") int userId) {
        User tempUser = userService.findUserById(userId);

        if (tempUser.getRole().getRoleName().equals("ROLE_USER")) {
            userService.setRole(tempUser.getUsername(), "ROLE_ADMIN");
        } else {
            userService.setRole(tempUser.getUsername(), "ROLE_USER");
        }

        userService.save(tempUser);

        return "redirect:/admin/users/list";
    }

    @PostMapping("/editingUserProcessing")
    public String editUser(@Valid @ModelAttribute("user") User userFromForm,
                           BindingResult bindingUserResult,
                           @Valid @ModelAttribute("userDetails") UserDetails userDetailsFromForm,
                           BindingResult bindingUserDetailsResult,
                           Model model,
                           RedirectAttributes redirectAttributes) {
        User oldUser = userService.findUserByUsername(userFromForm.getUsername());
        UserDetails oldUserDetails = oldUser.getUserDetails();
        if (userService.checkingPasswordIsValidFromForm(userFromForm.getPassword(), userFromForm.getPasswordRepeat())) {

            if (userService.checkingEmailIsNewAndNotExist(userDetailsFromForm.getEmail(), oldUserDetails.getEmail())) {
                return "admin/userEditForm";
            }

            userService.editUser(userFromForm.getUsername(), userFromForm, userDetailsFromForm);
            redirectAttributes.addFlashAttribute("editedUserSuccessfully", "You edited the user successfully!");
            return "redirect:/admin/users/edit/form?userId=" + oldUser.getUserId();
        }

        return "admin/userEditForm";
    }

}

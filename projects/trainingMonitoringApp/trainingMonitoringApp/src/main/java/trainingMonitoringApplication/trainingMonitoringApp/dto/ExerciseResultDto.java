package trainingMonitoringApplication.trainingMonitoringApp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class ExerciseResultDto {

    int idExerciseResult;
    private List<Double> weight;
    private List<Integer> repetitions;
    private String date;


    public ExerciseResultDto(int idExerciseResult, List<Number> weight, List<Number> repetitions, String date) {
        this.idExerciseResult = idExerciseResult;
        this.weight = weight.parallelStream().mapToDouble(i-> (double) i)
                .boxed().collect(Collectors.toList());
        this.repetitions = repetitions.parallelStream().mapToInt(i-> (int) i)
                .boxed().collect(Collectors.toList());
        this.date=date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExerciseResultDto that = (ExerciseResultDto) o;
        return idExerciseResult == that.idExerciseResult && Objects.equals(weight, that.weight) && Objects.equals(repetitions, that.repetitions) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idExerciseResult, weight, repetitions, date);
    }
}

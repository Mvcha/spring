package trainingMonitoringApplication.trainingMonitoringApp.repository.exercise;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;

public interface ExercisesRepository extends JpaRepository<Exercise, Integer> {

    List<Exercise> findAll();
    List<Exercise> findByExercisesCategory(ExercisesCategory exercisesCategory);
    @Query(value = "SELECT e.name, e.exercise_id, e.category_id, e.is_own, e.user_id FROM exercises e INNER JOIN exercises_category ec on e.category_id=ec.exercise_category_id WHERE ec.name=?1",
            nativeQuery = true)
    List<Exercise> findExerciseByCategoryName(String categoryName);

    Exercise findByName(String name);
    Exercise findExerciseByExerciseId(int id);

    void deleteExerciseByExerciseId(int id);

    List<Exercise> findAllByUser(User user);

}

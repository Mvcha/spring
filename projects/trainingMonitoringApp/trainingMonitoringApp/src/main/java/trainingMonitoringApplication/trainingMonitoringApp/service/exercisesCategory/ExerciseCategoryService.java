package trainingMonitoringApplication.trainingMonitoringApp.service.exercisesCategory;

import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;

import java.util.List;

public interface ExerciseCategoryService {

    List<ExercisesCategory> findAll();

    List<String> findAllCategories();
    ExercisesCategory findByName(String name);


}

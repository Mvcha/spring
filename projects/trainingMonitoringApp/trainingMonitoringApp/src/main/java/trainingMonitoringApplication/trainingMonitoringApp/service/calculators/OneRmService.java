package trainingMonitoringApplication.trainingMonitoringApp.service.calculators;

import org.springframework.stereotype.Service;
import trainingMonitoringApplication.trainingMonitoringApp.model.OneRmData;

import java.util.ArrayList;
import java.util.List;

@Service
public class OneRmService {

    public Double calculateOneRM(double lift,
                                 int repetitions) {
        if(lift<=0 || repetitions<=0){
            throw new IllegalArgumentException("Lift or repetitions can't be equal or lower than 0");
        }
        double oneRm = (1 + (0.0333 * repetitions)) * lift;
        oneRm=Math.round(oneRm*10.0)/10.0;
        return oneRm;
    }

    public List<OneRmData> rmValues(Double oneRm) {
        List<OneRmData> rmResults = new ArrayList<>();
        rmResults.add(new OneRmData(100,oneRm, 1));
        int repetitions=2;
        for (int i = 95; i >= 70; i -= 5) {
            double percentage = (double) i / 100;
            double liftWeight=Math.round(oneRm*percentage*10.0)/10.0;
            rmResults.add(new OneRmData(i,liftWeight,repetitions));
            repetitions+=2;

        }
        return rmResults;
    }
}

package trainingMonitoringApplication.trainingMonitoringApp.entity.plan;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "exercises_result")
public class ExerciseResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="exercise_id")
    private Exercise exercise;
    @Column(name = "weight_json")
    private String weightJson;
    @Column(name = "repetition_json")
    private String repetitionJson;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "date")
    private String date;

    public ExerciseResult(Exercise exercise, String weightJson, String repetitionJson, User user, String date) {
        this.exercise = exercise;
        this.weightJson = weightJson;
        this.repetitionJson = repetitionJson;
        this.user = user;
        this.date = date;
    }


    @Override
    public String toString() {
        return "ExerciseResult{" +
                "id=" + id +
                ", exercise=" + exercise +
                ", weightJson='" + weightJson + '\'' +
                ", repetitionJson='" + repetitionJson + '\'' +
                ", user=" + user +
                ", date='" + date + '\'' +
                '}';
    }

}

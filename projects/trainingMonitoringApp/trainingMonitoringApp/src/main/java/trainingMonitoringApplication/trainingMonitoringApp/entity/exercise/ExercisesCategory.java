package trainingMonitoringApplication.trainingMonitoringApp.entity.exercise;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "exercises_category")
public class ExercisesCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exercise_category_id")
    private int exerciseId;

    @Column(name="name")
    private String name;

    @Override
    public String toString() {
        return "ExercisesCategory{" +
                "name='" + name + '\'' +
                '}';
    }

    public ExercisesCategory(int exerciseId, String name) {
        this.exerciseId = exerciseId;
        this.name = name;
    }
}

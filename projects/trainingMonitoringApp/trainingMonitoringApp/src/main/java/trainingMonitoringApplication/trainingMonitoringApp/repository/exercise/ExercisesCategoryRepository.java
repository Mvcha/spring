package trainingMonitoringApplication.trainingMonitoringApp.repository.exercise;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;

import java.util.List;


public interface ExercisesCategoryRepository extends JpaRepository <ExercisesCategory, Integer> {

    List<ExercisesCategory> findAll();
    ExercisesCategory findByName(String name);

}

package trainingMonitoringApplication.trainingMonitoringApp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultDto;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class ExercisesResultsForm {
    private Map<Integer, ExerciseResultDto> exerciseResults;

    @Override
    public String toString() {
        return "ExercisesResultsForm{" +
                "exerciseResults=" + exerciseResults +
                '}';
    }
}

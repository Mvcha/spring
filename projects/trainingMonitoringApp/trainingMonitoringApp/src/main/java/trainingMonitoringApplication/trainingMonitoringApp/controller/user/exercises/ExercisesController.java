package trainingMonitoringApplication.trainingMonitoringApp.controller.user.exercises;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercisesCategory.ExerciseCategoryService;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/user/exercises")
public class ExercisesController {

    private ExerciseService exerciseService;
    private ExerciseCategoryService exerciseCategoryService;
    private UserService userService;

    public ExercisesController(ExerciseService exerciseService,
                               ExerciseCategoryService exerciseCategoryService,
                               UserService userService) {
        this.exerciseService = exerciseService;
        this.exerciseCategoryService = exerciseCategoryService;
        this.userService = userService;
    }

    @GetMapping("/list")
    public String listExercises(Model model) {
        List<ExerciseDto> exercisesList = exerciseService.findAllNamesWithCategoryWithoutOwnExercise();
        model.addAttribute("exercisesList", exercisesList);

        return "user/exercises/exercisesList";
    }

    @GetMapping(value = "/ownList", headers = "Accept=application/json")
    public String ownListExercises(Model model,
                                   Principal principal) {


        List<ExerciseDto> exercisesList = exerciseService.findAllExercisesByUserAndListInDto(userService.findUserByUsername(principal.getName()));
        List<String> exercisesCategoryList = exerciseCategoryService.findAllCategories();
        model.addAttribute("exercisesList", exercisesList);
        model.addAttribute("exercisesCategoryList", exercisesCategoryList);
        return "user/exercises/ownExercisesList";
    }

    @GetMapping(value = "/ownExercise/{id}/delete")
    public String removeOwnExercise(@PathVariable("id") int id) {
        exerciseService.deleteExerciseByExerciseId(id);
        return "redirect:/user/exercises/ownList";
    }

    @GetMapping(value = "/ownExercise/{id}/edit")
    public String editOwnExercise(@PathVariable("id") int id,
                                  Model model,
                                  Principal principal) {
        Exercise ownExercise = exerciseService.findExerciseById(id);
        List<ExerciseDto> exercisesList = exerciseService.findAllExercisesByUserAndListInDto(userService.findUserByUsername(principal.getName()));
        List<String> exercisesCategoryList = exerciseCategoryService.findAllCategories();
        model.addAttribute("exercisesList", exercisesList);
        model.addAttribute("exercisesCategoryList", exercisesCategoryList);
        model.addAttribute("ownExercise", ownExercise);
        return "user/exercises/ownExercisesList";
    }
    @PostMapping(value = "/ownExercise/edit")
    public String editOwnExercise(@RequestParam(value = "exerciseId") int id,
                                 @RequestParam(value = "newName") String name,
                                 @RequestParam(value = "newCategory") String category,
                                 Principal principal) {

        exerciseService.editOwnExercise(id, name,category, userService.findUserByUsername(principal.getName()));
        return "redirect:/user/exercises/ownList";
    }
    @PostMapping(value = "/ownExercise/add")
    public String addOwnExercise(@RequestParam(value = "name") String name,
                                 @RequestParam(value = "category") String category,
                                 Principal principal) {

        exerciseService.addOwnExercise(name, category, userService.findUserByUsername(principal.getName()));
        return "redirect:/user/exercises/ownList";
    }

}

package trainingMonitoringApplication.trainingMonitoringApp.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExerciseResultChartDto {

    private String date;
    private Double maxWeight;

    public ExerciseResultChartDto(String date, Double maxWeight) {
        this.date = date;
        this.maxWeight = maxWeight;
    }

    @Override
    public String toString() {
        return "ExerciseResultChartDto{" +
                "date='" + date + '\'' +
                ", maxWeight=" + maxWeight +
                '}';
    }
}

package trainingMonitoringApplication.trainingMonitoringApp.entity.user;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="roles")
public class Roles {

    @Id
    @Column(name = "user_id")
    private int userId;

    @Column(name ="role")
    private String roleName;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private User user;

    public Roles(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "Roles{" +
                "userId=" + userId +
                ", role='" + roleName + '\'' +
                '}';
    }
}

package trainingMonitoringApplication.trainingMonitoringApp.repository.exerciseResults;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.ExerciseResult;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;

public interface ExerciseResultRepository extends JpaRepository<ExerciseResult, Integer> {

    List<ExerciseResult> findByUser(User user);
    List<ExerciseResult> findByUserAndExerciseOrderByDate(User user, Exercise exercise);
}

package trainingMonitoringApplication.trainingMonitoringApp.service.calculators;

import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class MassCalculatorService {


    public BigDecimal bmiFactor(BigDecimal weight,
                                BigDecimal height) {

        if (weight.compareTo(BigDecimal.ZERO) <= 0 || height.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Weight or height cannot be lower than 0");
        }
        BigDecimal bmi = weight.divide(height.divide(
                        new BigDecimal(100), 2, RoundingMode.HALF_UP).pow(2),
                2, RoundingMode.HALF_UP);
        return bmi.setScale(2, RoundingMode.UP);
    }

    public String bmiResult(double bmiFactor) {
        if (bmiFactor < 16) {
            return "underweight II";
        } else if (bmiFactor <= 18.49) {
            return "underweight";
        } else if (bmiFactor <= 24.99) {
            return "OK";
        } else if (bmiFactor <= 29.9) {
            return "overweight";
        } else {
            return "obesity";
        }
    }

    public int rfmFactor(String gender,
                         BigDecimal height,
                         BigDecimal waist) {

        if (waist.compareTo(BigDecimal.ZERO) <= 0 || height.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Waist or height cannot be lower than 0");
        }

        double v = 20 * (height.doubleValue() / waist.doubleValue());
        if (gender.equals("Male")) {
            return (int) (64 - v);
        } else {
            return (int) (76 - v);
        }
    }

    public String rfmResult(int rfmFactor,
                            String gender,
                            int age) {
        if (gender.equals("Male")) {
            if (age > 18) {
                if (rfmFactor < 7) {
                    return "underweight";
                } else if (rfmFactor < 20) {
                    return "OK";
                } else if (rfmFactor <= 25) {
                    return "overweight";
                } else if (rfmFactor > 25) {
                    return "obesity";
                }
            } else {
                if (rfmFactor <= 11) {
                    return "underweight";
                } else if (rfmFactor < 21) {
                    return "OK";
                } else if (rfmFactor <= 27) {
                    return "overweight";
                } else if (rfmFactor > 27) {
                    return "obesity";
                }
            }

        } else {
            if (age > 18) {
                if (rfmFactor < 21) {
                    return "underweight";
                } else if (rfmFactor < 33) {
                    return "OK";
                } else if (rfmFactor <= 38) {
                    return "overweight";
                } else if (rfmFactor > 38) {
                    return "obesity";
                }
            } else {
                if (rfmFactor <= 15) {
                    return "underweight";
                } else if (rfmFactor < 30) {
                    return "OK";
                } else if (rfmFactor <= 34) {
                    return "overweight";
                } else if (rfmFactor > 34) {
                    return "obesity";
                }
            }
        }
        throw new IllegalArgumentException("Age can't be lower than 1");
    }

}

package trainingMonitoringApplication.trainingMonitoringApp.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Integer> {

    UserDetails findByEmail(String email);

}

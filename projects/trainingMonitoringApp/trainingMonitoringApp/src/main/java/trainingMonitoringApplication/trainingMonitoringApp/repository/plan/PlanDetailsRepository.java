package trainingMonitoringApplication.trainingMonitoringApp.repository.plan;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.PlanDetails;

import java.util.List;
import java.util.Optional;

public interface PlanDetailsRepository extends JpaRepository <PlanDetails, Integer> {

    List<PlanDetails> findByPlan(Plan plan);
    List<PlanDetails> findAllByPlan(Plan plan);
    PlanDetails findByPlanAndExercise(Plan plan, Exercise exercise);
}

package trainingMonitoringApplication.trainingMonitoringApp.service.exerciseResults;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultChartDto;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.ExerciseResult;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.repository.exerciseResults.ExerciseResultRepository;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExerciseResultServiceImpl implements ExerciseResultService {
    private final ObjectMapper objectMapper;
    private final ExerciseResultRepository exerciseResultRepository;
    private final ExerciseService exerciseService;
    private static final LocalDate currentDate = LocalDate.now();
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


    public ExerciseResultServiceImpl(ObjectMapper objectMapper, ExerciseResultRepository exerciseResultRepository, ExerciseService exerciseService) {
        this.objectMapper = objectMapper;
        this.exerciseResultRepository = exerciseResultRepository;
        this.exerciseService = exerciseService;
    }

    //Create json from list
    public String convertListToJson(List list) throws JsonProcessingException {
        return objectMapper.writeValueAsString(list);
    }

    //Read values repetitions or weights from json and convert to list
    public List<Number> convertJsonToNumberList(String json) throws JsonProcessingException {

        return objectMapper.readValue(json, new TypeReference<List<Number>>() {
        });
    }


    //Checking map. If map includes nulls than they are deleted
    @Override
    public Map<Integer, ExerciseResultDto> deleteNullsFromExerciseResultMap(Map map) {
        Set<Map.Entry<Integer, ExerciseResultDto>> entries = map.entrySet();
        Iterator<Map.Entry<Integer, ExerciseResultDto>> mapIterator = entries.iterator();
        List<Integer> keysToRemove = new ArrayList<>();

        while (mapIterator.hasNext()) {
            Map.Entry<Integer, ExerciseResultDto> entry = mapIterator.next();
            if (exerciseResultIsEmpty(entry)) {
                //if weight or repetition is null than add key to keysToRemove, so we'll delete entire exercise
                //It means exercise wasn't performed
                keysToRemove.add(entry.getKey());
            } else {
                //Removing series in exercise
                removeNullInSeries(entry);
            }
        }
        for (Integer key : keysToRemove) {
            map.remove(key);
        }

        return map;
    }

    private Boolean exerciseResultIsEmpty(Map.Entry<Integer, ExerciseResultDto> entry) {
        return entry.getValue().getWeight().stream().allMatch(weight -> weight == null)
                || entry.getValue().getRepetitions().stream().allMatch(repetitions -> repetitions == null);
    }

    private void removeNullInSeries(Map.Entry<Integer, ExerciseResultDto> entry) {
        Set<Integer> seriesToDelete = new HashSet<>();
        for (int i = 0; i < entry.getValue().getWeight().size(); i++) {
            if (entry.getValue().getWeight().get(i) == null) {
                //adding the series to the list where weight wasn't given
                seriesToDelete.add(i);
            }
        }
        for (int i = 0; i < entry.getValue().getRepetitions().size(); i++) {
            //adding the series to the list where repetitions wasn't given

            if (entry.getValue().getRepetitions().get(i) == null) {
                seriesToDelete.add(i);
            }
        }
        List<Integer> reverseSeriesToDelete = seriesToDelete.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        for (int i : reverseSeriesToDelete) {
            entry.getValue().getWeight().remove(i);
            entry.getValue().getRepetitions().remove(i);
        }
    }

    //Exercise results for one exercise
    @Override
    public LinkedHashMap<Exercise, List<ExerciseResult>> showExerciseResults(User user) {

        //collecting results to one list
        List<ExerciseResult> exerciseResultList = exerciseResultRepository.findByUser(user);

        //stream and grouping by exercise and results for this exercise
        return exerciseResultList.stream()
                .collect(Collectors.groupingBy(ExerciseResult::getExercise, LinkedHashMap::new, Collectors.toList()));
    }

    //show only results for one exercise
    @Override
    public List<ExerciseResultDto> showResultsForExercise(User user, Exercise exercise) {

        List<ExerciseResult> listOfExercisesResults = exerciseResultRepository.findByUserAndExerciseOrderByDate(user, exercise);

        return listOfExercisesResults.stream()
                .map(result -> {
                    try {
                        return new ExerciseResultDto(result.getId(),
                                convertJsonToNumberList(result.getWeightJson()),
                                convertJsonToNumberList(result.getRepetitionJson()),
                                result.getDate());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());


    }


    @Override
    public void save(ExerciseResult exerciseResult) {
        exerciseResultRepository.save(exerciseResult);
    }

    //convert exercise result chart dto to json. It'll be use in charts
    @Override
    public String convertListOfObjectsToJson(List<ExerciseResultChartDto> exerciseResultChartDto) throws JsonProcessingException {
        return objectMapper.writeValueAsString(exerciseResultChartDto);
    }

    @Override
    public List<ExerciseResultChartDto> exerciseResultDtoToExerciseResultChartDto(List<ExerciseResultDto> exerciseResultDto) {

        List<ExerciseResultChartDto> list = new ArrayList<>();
        exerciseResultDto.forEach(exerciseResult -> list.add(new ExerciseResultChartDto(
                exerciseResult.getDate(),
                exerciseResult.getWeight()
                        .stream()
                        .mapToDouble(i -> i)
                        .max()
                        .getAsDouble()
        )));

        return list;
    }


    //saving results
    @Override
    public void save(User user, Map map) throws JsonProcessingException {
        //remove nulls from map
        Map mapWithoutNulls = deleteNullsFromExerciseResultMap(map);
        Set<Map.Entry<Integer, ExerciseResultDto>> entries = mapWithoutNulls.entrySet();
        Iterator<Map.Entry<Integer, ExerciseResultDto>> mapIterator = entries.iterator();

        while (mapIterator.hasNext()) {
            Map.Entry<Integer, ExerciseResultDto> entry = mapIterator.next();
            exerciseResultRepository.save(new ExerciseResult(
                    exerciseService.findExerciseById(entry.getValue().getIdExerciseResult()),
                    convertListToJson(entry.getValue().getWeight()),
                    convertListToJson(entry.getValue().getRepetitions()),
                    user,
                    currentDate.format(dateTimeFormatter)));
        }
    }

}

package trainingMonitoringApplication.trainingMonitoringApp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class OneRmData {
    private int percentage;
    private double lift;
    private int repetitions;


    public OneRmData(int percentage, double lift, int repetitions) {
        this.percentage = percentage;
        this.lift = lift;
        this.repetitions = repetitions;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OneRmData oneRmData = (OneRmData) o;
        return percentage == oneRmData.percentage && Double.compare(oneRmData.lift, lift) == 0 && repetitions == oneRmData.repetitions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(percentage, lift, repetitions);
    }

    @Override
    public String toString() {
        return "OneRmData{" +
                "percentage=" + percentage +
                ", lift=" + lift +
                ", repetitions=" + repetitions +
                '}';
    }
}



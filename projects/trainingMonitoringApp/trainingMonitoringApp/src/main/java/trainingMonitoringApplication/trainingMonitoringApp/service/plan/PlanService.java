package trainingMonitoringApplication.trainingMonitoringApp.service.plan;

import trainingMonitoringApplication.trainingMonitoringApp.dto.PlanDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;

public interface PlanService {
    void save(Plan plan);
    List<Plan> findAllByUser(User user);
    Plan findByPlanIdAndUser(int planId, User user);
    void deleteByPlanId(int planId);
    List<PlanDto>findAllPlansByUser(User user);
    PlanDto planDtoByPlanId(int planId, User user);
}

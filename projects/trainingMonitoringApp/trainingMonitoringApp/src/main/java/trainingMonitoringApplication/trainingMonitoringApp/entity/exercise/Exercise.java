package trainingMonitoringApplication.trainingMonitoringApp.entity.exercise;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "exercises")
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "exercise_id")
    private int exerciseId;

    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "category_id")
    private ExercisesCategory exercisesCategory;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "is_own")
    private boolean isOwn;


        public Exercise(int exerciseId, String name, ExercisesCategory exercisesCategory) {
            this.exerciseId = exerciseId;
            this.name = name;
            this.exercisesCategory = exercisesCategory;
        }
    public Exercise(String name, ExercisesCategory exercisesCategory) {
        this.name = name;
        this.exercisesCategory = exercisesCategory;
    }

    public Exercise(String name, ExercisesCategory exercisesCategory, User user, boolean isOwn) {
        this.name = name;
        this.exercisesCategory = exercisesCategory;
        this.user = user;
        this.isOwn = isOwn;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "exerciseId=" + exerciseId +
                ", name='" + name + '\'' +
                ", exercisesCategory=" + exercisesCategory +
                ", user=" + user +
                ", isOwn=" + isOwn +
                '}';
    }

    public int getUserId() {
        if (user != null) {
            return user.getUserId();
        }
        return 0;
    }
}

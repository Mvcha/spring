package trainingMonitoringApplication.trainingMonitoringApp.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingAspect {

    private static final Logger logger =
            LoggerFactory.getLogger(LoggingAspect.class);

    @Before("execution(* trainingMonitoringApplication.trainingMonitoringApp.controller.admin.*.*(..))")
    public void checkAdmin() {
    }

    @Pointcut("within (trainingMonitoringApplication.trainingMonitoringApp.controller..*)")
    private void forControllerPackage() {
    }

    @Pointcut("execution(* trainingMonitoringApplication.trainingMonitoringApp.service.*.*.*(..))")
    private void forServicePackage() {
    }

    @Pointcut("forControllerPackage() || forServicePackage()")
    private void forAppFlow() {
    }

    @Before("forAppFlow()")
    public void before(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().toShortString();
        logger.info("=====> Calling method: " + method);

        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            logger.info("===> Argument: "+arg);
        }
    }

    @AfterReturning(pointcut = "forAppFlow()",
    returning = "result")
    public void afterMethods(JoinPoint joinPoint, Object result){
        String method = joinPoint.getSignature().toShortString();
        logger.info("=====> After method: " + method);
        logger.info("===> Result: "+result);
    }


}

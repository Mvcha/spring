package trainingMonitoringApplication.trainingMonitoringApp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ExerciseDto {
    private int id;
    private String name;
    private String categoryName;


    public ExerciseDto(int id, String name, String categoryName) {
        this.id = id;
        this.name = name;
        this.categoryName = categoryName;
    }

    public ExerciseDto(String name, String categoryName) {
        this.name = name;
        this.categoryName = categoryName;
    }

}

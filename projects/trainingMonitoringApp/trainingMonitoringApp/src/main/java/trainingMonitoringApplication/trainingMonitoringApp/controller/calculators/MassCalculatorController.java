package trainingMonitoringApplication.trainingMonitoringApp.controller.calculators;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import trainingMonitoringApplication.trainingMonitoringApp.service.calculators.MassCalculatorService;

import java.math.BigDecimal;


@Controller
public class MassCalculatorController {
    private final MassCalculatorService massCalculatorService;

    public MassCalculatorController(MassCalculatorService massCalculatorService) {
        this.massCalculatorService = massCalculatorService;
    }

    @GetMapping("/massIndicatorCalculator")
    public String showMassIndicatorCalculator() {

        return "calculators/massIndicatorCalculator";
    }

    @PostMapping("/calculateMassIndicator")
    public String calculateBmi(@RequestParam(value = "age", required = false) Integer age,
                               @RequestParam(value = "height", required = false) BigDecimal height,
                               @RequestParam(value = "weight", required = false) BigDecimal weight,
                               @RequestParam(value = "waist", required = false) BigDecimal waist,
                               @RequestParam(value = "gender", required = false) String gender,
                               Model model) {

            BigDecimal bmi=massCalculatorService.bmiFactor(weight,height);
            String classificationOfObesityBmi = massCalculatorService.bmiResult(bmi.doubleValue());

            model.addAttribute("bmi", bmi);
            model.addAttribute("classificationOfObesityBmi", classificationOfObesityBmi);

        if (waist != null && height != null) {
            int rfm = massCalculatorService.rfmFactor(gender, height, waist);
            String classificationOfObesityRfm = massCalculatorService.rfmResult(rfm, gender, age);
            model.addAttribute("rfm", rfm);
            model.addAttribute("classificationOfObesityRfm", classificationOfObesityRfm);
        }


        model.addAttribute("age", age);
        model.addAttribute("height", height);
        model.addAttribute("weight", weight);
        model.addAttribute("waist", waist);
        model.addAttribute("gender", gender);

        return "calculators/massIndicatorCalculator";

    }
    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalArgumentException(Model model, IllegalArgumentException ex) {
        model.addAttribute("error", ex.getMessage());
        return "calculators/massIndicatorCalculator";
    }
}

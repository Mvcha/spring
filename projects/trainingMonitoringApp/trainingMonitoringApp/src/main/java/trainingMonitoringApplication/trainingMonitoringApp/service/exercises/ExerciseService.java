package trainingMonitoringApplication.trainingMonitoringApp.service.exercises;

import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;

public interface ExerciseService {
    Exercise findExerciseById(int id);
    List<Exercise> findAll();
    List<ExerciseDto> findAllNamesWithCategoryWithoutOwnExercise();
    List<Exercise> findByExercisesCategory(ExercisesCategory exercisesCategory);
    List<List<Exercise>> listExercisesByCategory();
    List<Exercise> findExerciseByCategoryName(String categoryName, int userId);
    Exercise findByName(String name);
    void deleteExerciseByExerciseId(int id);
    void addOwnExercise(String name, String category, User user);
    void editOwnExercise(int exerciseId, String name, String category, User user);

    List<Exercise> findAllExercisesByUser(User user);
    List<ExerciseDto> findAllExercisesByUserAndListInDto(User user);
}

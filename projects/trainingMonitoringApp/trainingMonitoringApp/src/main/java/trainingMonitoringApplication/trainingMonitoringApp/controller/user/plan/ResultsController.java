package trainingMonitoringApplication.trainingMonitoringApp.controller.user.plan;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultChartDto;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseResultDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.ExerciseResult;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.PlanDetails;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.model.ExercisesResultsForm;
import trainingMonitoringApplication.trainingMonitoringApp.service.exerciseResults.ExerciseResultService;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercises.ExerciseService;
import trainingMonitoringApplication.trainingMonitoringApp.service.plan.PlanDetailsService;
import trainingMonitoringApplication.trainingMonitoringApp.service.plan.PlanService;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/user/plan/results")
public class ResultsController {

    private final PlanService planService;
    private final PlanDetailsService planDetailsService;
    private final ExerciseResultService exerciseResultService;
    private final UserService userService;
    private final ExerciseService exerciseService;

    public ResultsController(PlanService planService,
                             PlanDetailsService planDetailsService,
                             ExerciseResultService exerciseResultService,
                             UserService userService,
                             ExerciseService exerciseService) {
        this.planService = planService;
        this.planDetailsService = planDetailsService;
        this.exerciseResultService = exerciseResultService;
        this.userService = userService;
        this.exerciseService = exerciseService;
    }

    @GetMapping("/run/{id}")
    public String doWorkout(@PathVariable("id") int planId,
                          ModelMap map, Model model, Principal principal)
    {
        User user=userService.findUserByUsername(principal.getName());
        List<PlanDetails> listOfPlanDetails = planDetailsService.findAllByPlan(planService.findByPlanIdAndUser(planId,user));

        if (listOfPlanDetails.isEmpty())
        {
            return "redirect:/user/plan/prepared/list";
        }
        Map<String, List<ExerciseDto>> mapExercisesByCategory= planDetailsService.groupExerciseByCategoryFromListOfExercises(
                planDetailsService.listOfExercises(listOfPlanDetails));
        map.addAttribute("mapExercisesByCategory", mapExercisesByCategory);
        model.addAttribute("planId", planId);
        model.addAttribute("elementsNumber",new ArrayList<>(15));
        model.addAttribute("exercisesResultsForm", new ExercisesResultsForm());
        model.addAttribute("exercisesResults", new ExerciseResultDto());
        return "user/trainingPlan/results/workoutProgressForm";
    }

    @PostMapping("/proceed/{id}")
    public String proceedFormWithResults(@PathVariable("id") int planId,
                                         @ModelAttribute("exercisesResultsForm") ExercisesResultsForm exercisesResultsForm,
                                         Principal principal) throws JsonProcessingException {

        Map<Integer, ExerciseResultDto> map=exercisesResultsForm.getExerciseResults();

        exerciseResultService.save(userService.findUserByUsername(principal.getName()), map);

        return "redirect:/user/plan/prepared/list";
    }


    @GetMapping("/list")
    public String showResultsList(Principal principal,
                                  ModelMap map,
                                  Model model) {

        LinkedHashMap<Exercise, List<ExerciseResult>> resultsList = exerciseResultService.showExerciseResults(userService.findUserByUsername(principal.getName()));

        map.addAttribute("mapExercisesByCategory", resultsList);

        return "user/trainingPlan/results/listExercisesToShowResults";
    }

    @GetMapping("/get/{id}")
    public String showResultsByExercise(@PathVariable int id,
                                        Principal principal, Model model) throws JsonProcessingException {
        User user=userService.findUserByUsername(principal.getName());
        Exercise exercise=exerciseService.findExerciseById(id);
        List<ExerciseResultDto> exerciseResult=exerciseResultService.showResultsForExercise(user, exercise);
        if(exerciseResult.isEmpty())
        {
            return "redirect:/user/plan/results/list";
        }
        List<ExerciseResultChartDto> exerciseResultChartDto=exerciseResultService.exerciseResultDtoToExerciseResultChartDto(exerciseResult);
        String jsonResults = exerciseResultService.convertListOfObjectsToJson(exerciseResultChartDto);
        model.addAttribute("exerciseResult", exerciseResult);
        model.addAttribute("exercise",exercise);
        model.addAttribute("jsonResults", jsonResults);
        return "user/trainingPlan/results/showResults";
    }
}

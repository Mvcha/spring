package trainingMonitoringApplication.trainingMonitoringApp.entity.user;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="users_details")
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_detail_id")
    private int userDetailId;
    @Size(min=1, message="First name cannot be empty ")
    @Column(name="first_name")
    private String firstName;
    @Size(min=1, message= "Last name cannot be empty")
    @Column(name="last_name")
    private String lastName;
    @Size(min=1, message = "E-mail cannot be empty")
    @Column(name="email",  unique = true)
    @Email(message = "E-mail is incorrect ")
    private String email;
    @Column(name="gender")
    private String gender;
    @Column(name="birthday")
    private String birthday;
    @Column(name="registration_date")
    private String registrationDate;


    public UserDetails(String firstName, String lastName, String email, String gender, String birthday, String registrationDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.gender = gender;
        this.birthday = birthday;
        this.registrationDate = registrationDate;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", birthday='" + birthday + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                '}';
    }
}

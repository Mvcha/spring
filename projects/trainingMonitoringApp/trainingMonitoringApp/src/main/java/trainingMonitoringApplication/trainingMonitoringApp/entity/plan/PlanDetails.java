package trainingMonitoringApplication.trainingMonitoringApp.entity.plan;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "plan_details")
public class PlanDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plan_details_id")
    private int planDetailsId;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "plan_id")
    private Plan plan;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "exercise_id")
    private Exercise exercise;

    @Column(name = "is_own")
    private boolean isOwn;


    public PlanDetails(Plan plan, Exercise exercise, boolean isOwn) {
        this.plan = plan;
        this.exercise = exercise;
        this.isOwn = isOwn;
    }


}

package trainingMonitoringApplication.trainingMonitoringApp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trainingMonitoringApplication.trainingMonitoringApp.dto.UsersListDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.Roles;
import trainingMonitoringApplication.trainingMonitoringApp.repository.user.UserRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final UserDetailsService userDetailsService;
    private static final LocalDate currentDate = LocalDate.now();
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, UserDetailsService userDetailsService) {
        this.userRepository = userRepository;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public User findUserById(int id) {
        return userRepository.findById(id);
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    public void save(User user) {
        userRepository.save(user);

    }

    @Override
    public String encodeUserPassword(String password) {
        return passwordEncoder.encode(password);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<UsersListDto> findAllUsersWithDetails() {
        List<User> allUsers=findAll();

        return  allUsers.stream()
                .map(user -> new UsersListDto(user.getUserId(),
                        user.getUsername(),
                        user.getUserDetails().getEmail(),
                        user.isActive(),
                        user.getRole().getRoleName()))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void editUser(String username, User newUser, UserDetails newUserDetails) {
        User tempUser=findUserByUsername(username);
        UserDetails tempUserDetails= tempUser.getUserDetails();
        tempUser.setPassword(encodeUserPassword(newUser.getPassword()));

        tempUserDetails.setFirstName(newUserDetails.getFirstName());
        tempUserDetails.setLastName(newUserDetails.getLastName());
        tempUserDetails.setEmail(newUserDetails.getEmail());
        tempUserDetails.setGender(newUserDetails.getGender());
        tempUserDetails.setBirthday(newUserDetails.getBirthday());
        tempUser.setUserDetails(tempUserDetails);

        userRepository.save(tempUser);
    }

    @Override
    public void setRole(String username, String roleName) {
        User user = findUserByUsername(username);
        Roles role = user.getRole();
        role.setRoleName(roleName);
        user.setRole(role);
        save(user);
    }

    @Override
    public void registerNewUser(User user, UserDetails userDetails) {
        user.setActive(true);
        userDetails.setRegistrationDate(currentDate.format(dateTimeFormatter));
        user.setUserDetails(userDetails);
        user.setPassword(encodeUserPassword(user.getPassword()));
        Roles roles = new Roles("ROLE_USER");
        user.setRole(roles);
        roles.setUser(user);

        save(user);
    }

    @Override
    public boolean checkingPasswordIsValidFromForm(String password, String secondPassword) {

        if (password.equals(secondPassword) && !password.isEmpty() && !secondPassword.isEmpty())
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkingEmailIsNewAndNotExist(String newEmail, String oldEmail) {
        if(!newEmail.equals(oldEmail) && userDetailsService.findByEmail(newEmail)!=null)
        {
            return true;
        }
        return false;
    }

}

package trainingMonitoringApplication.trainingMonitoringApp.controller.calculators;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import trainingMonitoringApplication.trainingMonitoringApp.model.OneRmData;
import trainingMonitoringApplication.trainingMonitoringApp.service.calculators.OneRmService;

import java.util.List;

@Controller
public class OneRmController {

    private final OneRmService oneRmService;

    public OneRmController(OneRmService oneRmService) {
        this.oneRmService = oneRmService;
    }

    @GetMapping("/oneRmCalculator")
    public String showOneRMCalculator() {

        return "calculators/oneRmCalculator";
    }

    @PostMapping("/calculateOneRm")
    public String calculateOneRm(@RequestParam(value = "lift", required = false) Double lift,
                                 @RequestParam(value = "unit", required = false) String unit,
                                 @RequestParam(value = "repetitions", required = false) Integer repetitions,
                                 Model model) {

        if (lift == null || repetitions == null) {
            return "calculators/oneRmCalculator";
        }
        Double oneRM = oneRmService.calculateOneRM(lift, repetitions);
        model.addAttribute("oneRm", oneRM);

        List<OneRmData> numberOfWeights = oneRmService.rmValues(oneRM);
        model.addAttribute("OneRmData", numberOfWeights);


        return "calculators/oneRmCalculator";
    }

}

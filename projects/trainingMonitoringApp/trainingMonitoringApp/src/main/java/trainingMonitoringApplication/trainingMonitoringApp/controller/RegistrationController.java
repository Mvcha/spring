package trainingMonitoringApplication.trainingMonitoringApp.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserDetailsService;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

@Controller
public class RegistrationController {

    private UserService userService;
    private UserDetailsService userDetailsService;


    public RegistrationController(UserService userService,
                                  UserDetailsService userDetailsService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model) {
        model.addAttribute("user",new User());
        model.addAttribute("userDetails", new UserDetails());
        return "registrationForm";
    }
    @PostMapping("/register/save")
    public String registerNewUser(
            @Valid @ModelAttribute("user") User user,
            BindingResult bindingUserResult,
            @Valid @ModelAttribute("userDetails") UserDetails userDetails,
            BindingResult bindingUserDetailsResult,
            Model model,
            RedirectAttributes redirectAttributes) {


        if(userService.findUserByUsername(user.getUsername())!=null)
        {
            model.addAttribute("userExist", user.getUsername());
            return "registrationForm";
        }
        if(userDetailsService.findByEmail(userDetails.getEmail())!=null)
        {
            model.addAttribute("emailExist", userDetails.getEmail());
            return "registrationForm";
        }

        if(!user.getPassword().equals(user.getPasswordRepeat()))
        {
            model.addAttribute("passwordRepeat", "Repeat password is different");
            return "registrationForm";
        }
        if (bindingUserResult.hasErrors() || bindingUserDetailsResult.hasErrors()) {
            return "registrationForm";
        } else {
            userService.registerNewUser(user, userDetails);
            redirectAttributes.addFlashAttribute("message", "You have been registered successfully!");
            return "redirect:/loginPage";
        }

    }

}

package trainingMonitoringApplication.trainingMonitoringApp.service.plan;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import trainingMonitoringApplication.trainingMonitoringApp.dto.PlanDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.repository.plan.PlanRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlanServiceImpl implements PlanService {

    private final PlanRepository planRepository;
    private final PlanDetailsService planDetailsService;

    public PlanServiceImpl(PlanRepository planRepository, PlanDetailsService planDetailsService) {
        this.planRepository = planRepository;
        this.planDetailsService = planDetailsService;
    }

    @Override
    public void save(Plan plan) {

        planRepository.save(plan);
    }

    @Override
    public List<Plan> findAllByUser(User user) {

        return planRepository.findAllByUser(user);
    }

    @Override
    public Plan findByPlanIdAndUser(int planId, User user) {
        return planRepository.findByPlanIdAndUser(planId, user);
    }

    @Transactional
    @Override
    public void deleteByPlanId(int planId) {
        planRepository.deleteByPlanId(planId);
    }

    @Override
    public List<PlanDto> findAllPlansByUser(User user) {
        List<Plan> plans=findAllByUser(user);
        return plans.stream()
                .map(plan -> new PlanDto(plan.getPlanId(),
                        plan.getName(),
                        plan.getDescription(),
                        planDetailsService.isExist(plan)))
                .collect(Collectors.toList());
    }

    @Override
    public PlanDto planDtoByPlanId(int planId, User user) {
        Plan plan = findByPlanIdAndUser(planId, user);

        return  new PlanDto(planId,plan.getName(),plan.getDescription(), null);
    }
}

package trainingMonitoringApplication.trainingMonitoringApp.service.exercisesCategory;

import org.springframework.stereotype.Service;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;
import trainingMonitoringApplication.trainingMonitoringApp.repository.exercise.ExercisesCategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExerciseCategoryServiceImpl implements ExerciseCategoryService {

    private ExercisesCategoryRepository exercisesCategoryRepository;

    public ExerciseCategoryServiceImpl(ExercisesCategoryRepository exercisesCategoryRepository) {
        this.exercisesCategoryRepository = exercisesCategoryRepository;
    }

    @Override
    public List<ExercisesCategory> findAll() {
        return exercisesCategoryRepository.findAll();
    }

    @Override
    public List<String> findAllCategories() {
        return findAll().stream()
                .map(allCategories-> allCategories.getName())
                .collect(Collectors.toList());
    }

    @Override
    public ExercisesCategory findByName(String name) {
        return exercisesCategoryRepository.findByName(name);
    }


}

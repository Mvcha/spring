package trainingMonitoringApplication.trainingMonitoringApp.dto;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PlanDto {

    private int planId;
    private String name;
    private String description;
    private Boolean isFilled;


    public PlanDto(int planId, String name, String description, Boolean isFilled) {
        this.planId = planId;
        this.name = name;
        this.description = description;
        this.isFilled=isFilled;
    }
}

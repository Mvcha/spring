package trainingMonitoringApplication.trainingMonitoringApp.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class UsersListDto {
    private int id;
    private String username;
    private String email;
    private boolean isActive;
    private String role;


    public UsersListDto(int id, String username, String email, boolean isActive, String role) {
        this.id=id;
        this.username = username;
        this.email = email;
        this.isActive = isActive;
        this.role = role;
    }

}

package trainingMonitoringApplication.trainingMonitoringApp.service.plan;

import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.PlanDetails;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PlanDetailsService {
    void save(PlanDetails planDetails);
    void save(List<String> exercises, Plan plan);
    void removeUnchecked(List<PlanDetails> detailsLists, List <String>checkboxes);
    List<PlanDetails> findByPlan(Plan plan);
    Boolean isExist(Plan plan);
    List<PlanDetails> findAllByPlan(Plan plan);
    List<String> listOfExercises(List<PlanDetails>listOfPlanDetails);
    PlanDetails findByPlanAndExercise(Plan plan, Exercise exercise);
    Map<String, List<ExerciseDto>> groupExerciseByCategoryFromListOfExercises(List<String> listOfExercises);
}

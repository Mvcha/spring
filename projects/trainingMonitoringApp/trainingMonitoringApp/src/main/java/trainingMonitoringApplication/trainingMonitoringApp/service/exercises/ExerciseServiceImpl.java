package trainingMonitoringApplication.trainingMonitoringApp.service.exercises;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import trainingMonitoringApplication.trainingMonitoringApp.dto.ExerciseDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.Exercise;
import trainingMonitoringApplication.trainingMonitoringApp.entity.exercise.ExercisesCategory;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.repository.exercise.ExercisesRepository;
import trainingMonitoringApplication.trainingMonitoringApp.service.exercisesCategory.ExerciseCategoryService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExerciseServiceImpl implements ExerciseService {

    private ExercisesRepository exercisesRepository;
    private ExerciseCategoryService exerciseCategoryService;

    public ExerciseServiceImpl(ExercisesRepository exercisesRepository, ExerciseCategoryService exerciseCategoryService) {
        this.exercisesRepository = exercisesRepository;
        this.exerciseCategoryService = exerciseCategoryService;
    }

    @Override
    public Exercise findExerciseById(int id) {
        return exercisesRepository.findExerciseByExerciseId(id);
    }

    @Override
    public List<Exercise> findAll() {
        return exercisesRepository.findAll();
    }

    @Override
    public List<ExerciseDto> findAllNamesWithCategoryWithoutOwnExercise() {
        List<Exercise> exercisesListWithCategory = findAll();
        return exercisesListWithCategory.stream()
                .filter(exercise -> exercise.isOwn()==false)
                .map(exercise -> new ExerciseDto(exercise.getName(), exercise.getExercisesCategory().getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Exercise> findByExercisesCategory(ExercisesCategory exercisesCategory) {
        return exercisesRepository.findByExercisesCategory(exercisesCategory);
    }

    @Override
    public List<List<Exercise>> listExercisesByCategory() {
        List<Exercise> exercises=new ArrayList<>();
        List<String> allCategories = exerciseCategoryService.findAllCategories();
        for (String category : allCategories)
        {
            exercises.addAll(findByExercisesCategory(exerciseCategoryService.findByName(category)));
        }

        return null;
    }

    @Override
    public List<Exercise> findExerciseByCategoryName(String categoryName, int userId) {

        //grouping exercises by category and return list
        //filter if exercise User id equal null  (it means it standard exercise)
        // or exercise get user id is the same like user id (it means it's own exercise)
        return exercisesRepository.findExerciseByCategoryName(categoryName).stream()
                .filter(exercise -> Objects.isNull(exercise.getUser())
                        || exercise.getUserId()==userId)

                .collect(Collectors.toList());
    }

    @Override
    public Exercise findByName(String name) {
        return exercisesRepository.findByName(name);
    }

    @Override
    @Transactional
    public void deleteExerciseByExerciseId(int id) {
        exercisesRepository.deleteExerciseByExerciseId(id);
    }

    @Override
    public void addOwnExercise(String name, String category, User user) {
        exercisesRepository.save(new Exercise(name,exerciseCategoryService.findByName(category),user, true));
    }

    @Override
    public void editOwnExercise(int exerciseId, String name, String category, User user) {
        Exercise exercise=findExerciseById(exerciseId);
        exercise.setName(name);
        exercise.setExercisesCategory(exerciseCategoryService.findByName(category));
        exercisesRepository.save(exercise);
    }

    @Override
    public List<Exercise> findAllExercisesByUser(User user) {
        return exercisesRepository.findAllByUser(user);
    }

    @Override
    public List<ExerciseDto> findAllExercisesByUserAndListInDto(User user) {
        return findAllExercisesByUser(user).stream()
                .map(exercise -> new ExerciseDto(exercise.getExerciseId(),
                        exercise.getName(),
                        exercise.getExercisesCategory().getName()))
                .collect(Collectors.toList());
    }

}

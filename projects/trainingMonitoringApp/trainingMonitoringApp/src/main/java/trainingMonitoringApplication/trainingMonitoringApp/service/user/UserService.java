package trainingMonitoringApplication.trainingMonitoringApp.service.user;

import trainingMonitoringApplication.trainingMonitoringApp.dto.UsersListDto;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;

import java.util.List;

public interface UserService {

    User findUserById(int id);
    User findUserByUsername(String username);
    void save(User user);
    String encodeUserPassword(String password);
    List<User> findAll();
    List<UsersListDto> findAllUsersWithDetails();
    void editUser(String username, User newUser, UserDetails newUserDetails);
    void setRole(String username, String roleName);
    void registerNewUser(User user, UserDetails userDetails);
    boolean checkingPasswordIsValidFromForm(String password, String secondPassword);
    boolean checkingEmailIsNewAndNotExist(String newEmail, String oldEmail);

}

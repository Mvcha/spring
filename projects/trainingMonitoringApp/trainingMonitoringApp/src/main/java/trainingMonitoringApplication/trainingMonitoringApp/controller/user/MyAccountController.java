package trainingMonitoringApplication.trainingMonitoringApp.controller.user;

import jakarta.validation.Valid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.UserDetails;
import trainingMonitoringApplication.trainingMonitoringApp.service.user.UserService;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class MyAccountController {

    private UserService userService;


    public MyAccountController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/my-account")
    public String displayUserDetails(Model model) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userService.findUserByUsername(username);
        model.addAttribute("user", user);

        return "user/account/showAccountDetails";
    }

    @GetMapping("/edit")
    public String editOwnAccount(@RequestParam("userId") int userId,
                                 @Valid @ModelAttribute("user") User user,
                                 BindingResult bindingUserResult,
                                 @Valid @ModelAttribute("userDetails") UserDetails userDetails,
                                 BindingResult bindingUserDetailsResult,
                                 Model model,
                                 Principal principal) {

        User tempUser = userService.findUserById(userId);
        UserDetails tempUserDetails = tempUser.getUserDetails();
        if(tempUser.getUsername().equals(principal.getName()))
        {model.addAttribute("user", tempUser);
            model.addAttribute("userDetails", tempUserDetails);

            return "user/account/userEdit";

        }
        else {
            return "redirect:/user/my-account";
        }
    }

    @PostMapping("/editingUserProcessing")
    public String editUser(@Valid @ModelAttribute("user") User userFromForm,
                           BindingResult bindingUserResult,
                           @Valid @ModelAttribute("userDetails") UserDetails userDetailsFromForm,
                           BindingResult bindingUserDetailsResult,
                           Model model,
                           RedirectAttributes redirectAttributes) {
        User oldUser = userService.findUserByUsername(userFromForm.getUsername());
        UserDetails oldUserDetails = oldUser.getUserDetails();
        if (userService.checkingPasswordIsValidFromForm(userFromForm.getPassword(), userFromForm.getPasswordRepeat())) {

            if (userService.checkingEmailIsNewAndNotExist(userDetailsFromForm.getEmail(), oldUserDetails.getEmail())) {
                return "user/account/userEdit";
            }

            userService.editUser(userFromForm.getUsername(), userFromForm, userDetailsFromForm);
            redirectAttributes.addFlashAttribute("editedUserSuccessfully", "You edited the user successfully!");
            return "user/account/userEdit";
        }

        return "user/account/userEdit";
    }
}

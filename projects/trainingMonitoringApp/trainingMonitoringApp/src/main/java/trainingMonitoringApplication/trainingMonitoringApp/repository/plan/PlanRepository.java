package trainingMonitoringApplication.trainingMonitoringApp.repository.plan;

import org.springframework.data.jpa.repository.JpaRepository;
import trainingMonitoringApplication.trainingMonitoringApp.entity.plan.Plan;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;

import java.util.List;

public interface PlanRepository extends JpaRepository <Plan, Integer> {

    List<Plan> findAllByUser(User user);
    Plan findByPlanIdAndUser(int planId, User user);
    void deleteByPlanId(int planId);

}

package trainingMonitoringApplication.trainingMonitoringApp.entity.plan;

import jakarta.persistence.*;
import lombok.Data;
import trainingMonitoringApplication.trainingMonitoringApp.entity.user.User;



@Data
@Entity
@Table(name = "plan")
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plan_id")
    private int planId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;


}

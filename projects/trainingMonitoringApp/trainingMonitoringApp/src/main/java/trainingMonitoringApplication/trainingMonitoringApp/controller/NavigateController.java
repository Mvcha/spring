package trainingMonitoringApplication.trainingMonitoringApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class NavigateController {

    @GetMapping("/")
    public String showIndex() {
        return "index";
    }

    @GetMapping("/loginPage")
    public String showLoginForm() {
        return "loginPage";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {

        return "access-denied";
    }

    @GetMapping("/logged-in")
    public String successLogin() {
        return "logged-in";
    }

    @GetMapping("/logoutInfo")
    public String logout(RedirectAttributes redirectAttributes){
        redirectAttributes.addFlashAttribute("message", "You have been logged out successfully!");
        return "redirect:/";
    }
}


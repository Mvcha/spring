
const btn = document.getElementById('showFormButton');

btn.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('addNewExercise');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});

$(document).ready(function () {
    $('#table_id').DataTable({
        "ordering": true,
        "pagingType": "simple",
        "lengthMenu": [10, 20, 30],

    });
});

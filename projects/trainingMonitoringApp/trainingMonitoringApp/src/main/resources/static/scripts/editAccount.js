function hide()
{
    let errorParagraph=document.getElementById("errorMatching");
    errorParagraph.style.visibility='hidden';
}
function isEqual()
{
    let errorParagraph=document.getElementById("errorMatching");
    errorParagraph.style.visibility='visible';
    let password = document.getElementById("password").value;
    let passwordRepeat = document.getElementById("passwordRepeat").value;
    if (password != passwordRepeat) {

        errorParagraph.innerText="Password must be matching";
        document.getElementById("submitButton").disabled=true;
    }
    else
    {
        errorParagraph.innerText="";
        document.getElementById("submitButton").disabled=false;
    }

}
const xValues = ["2024-03-21","2024-03-22","2024-03-23","2024-03-24", "2024-03-25", "2024-03-21"];
const yValues = [7,8,8,9,9,9];

const jsonResults=JSON.parse(document.getElementById("jsonResults").getAttribute("data-json"));
console.log(jsonResults);


new Chart("myChart", {
    type: "line",
    data: {
        labels: jsonResults.map(row => row.date),
        datasets: [{
            label: 'Max weight per day',
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(0,0,255,1.0)",
            borderColor: "rgba(0,0,255,0.1)",
            data: jsonResults.map(row => row.maxWeight)
        }]
    }
});

const btnChest = document.getElementById('showChestExercises');
const btnBack = document.getElementById('showBackExercises');
const btnArms = document.getElementById('showArmsExercises');
const btnAbdominals = document.getElementById('showAbdominalsExercises');
const btnLegs = document.getElementById('showLegsExercises');
const btnShoulders = document.getElementById('showShouldersExercises');
const btnOwnExercises = document.getElementById('showOwnExercises');
btnChest.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('chestList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnBack.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('backList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnArms.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('armsList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnAbdominals.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('abdominalsList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnLegs.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('legsList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnShoulders.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('shouldersList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
btnOwnExercises.addEventListener('click', () => {
    const addNewExerciseDiv = document.getElementById('ownExercisesList');

    if (addNewExerciseDiv.style.display === 'none') {
        addNewExerciseDiv.style.display = 'block';
    } else {
        addNewExerciseDiv.style.display = 'none';
    }
});
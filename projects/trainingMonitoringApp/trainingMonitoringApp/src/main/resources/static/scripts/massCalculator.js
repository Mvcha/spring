
let bmiResult=document.getElementById("classificationOfObesityBmi").textContent;
let bmiProgressBar=document.getElementById("bmi-progress-bar");
if(bmiResult==="underweight II"){bmiProgressBar.style.width='10%'; bmiProgressBar.className='progress-bar bg-danger';}
if(bmiResult==="underweight"){bmiProgressBar.style.width='25%'; bmiProgressBar.className='progress-bar bg-warning';}
if(bmiResult==="OK"){bmiProgressBar.style.width='50%'; bmiProgressBar.className='progress-bar bg-success'; }
if(bmiResult==="overweight"){bmiProgressBar.style.width='75%';bmiProgressBar.className='progress-bar bg-warning';}
if(bmiResult==="obesity"){bmiProgressBar.style.width='100%'; bmiProgressBar.className='progress-bar bg-danger';}

let rfmResult=document.getElementById("classificationOfObesityRfm").textContent;
let rfmProgressBar=document.getElementById("rfm-progress-bar");
if(rfmResult==="underweight"){rfmProgressBar.style.width='10%'; rfmProgressBar.className='progress-bar bg-danger';}
if(rfmResult==="OK"){rfmProgressBar.style.width='50%'; rfmProgressBar.className='progress-bar bg-success'; }
if(rfmResult==="overweight"){rfmProgressBar.style.width='75%';rfmProgressBar.className='progress-bar bg-warning';}
if(rfmResult==="obesity"){rfmProgressBar.style.width='100%'; rfmProgressBar.className='progress-bar bg-danger';}


function clearForm() {
    document.getElementById("age").setAttribute("value", "");
    document.getElementById("weight").setAttribute("value", "");
    document.getElementById("height").setAttribute("value", "");
    document.getElementById("waist").setAttribute("value", "");
}
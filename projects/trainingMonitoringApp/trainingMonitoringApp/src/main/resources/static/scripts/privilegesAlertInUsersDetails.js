function privilegesAlert(role, username, userId) {
    if (role === "ROLE_USER") {
        if (confirm("Are you sure you want to give ADMIN privileges to " + username + "?")) {
            window.location.replace("/admin/users/updatePrivileges?userId=" + userId)
        }
    } else {
        if (confirm("Are you sure you want to give USER privileges to " + username + "?\n This user will lose access. ")) {
            window.location.replace("/admin/users/updatePrivileges?userId=" + userId)
        }
    }

}
package org.demo.productservice.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
public class ProductController {
   //private final ProductService productService;

/*
    public ProductController(ProductService productService) {
        this.productService = productService;
    }
*/

    @GetMapping("/test")
    public String testMethod()
    {
        return "test";
    }
}

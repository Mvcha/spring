package org.demo.productservice.service.products;


import org.demo.productservice.entity.Products;
import org.demo.productservice.model.factory.ClothesProduct;

import java.util.List;

public interface ProductService {
    List<Products> getProducts();
    Products getProduct(Long productId);

    void addClothes(ClothesProduct clothesProduct);
}

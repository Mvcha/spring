package org.demo.productservice.exception.productException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ProductExceptionHandler {

    @ExceptionHandler(ProductException.class)
    public ResponseEntity<ProductErrorInfo> productErrorResponseEntity (ProductException productException){
        switch (productException.getProductError()){
            case PRODUCT_ERROR -> {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ProductErrorInfo(productException.getProductError().getMessage()));
            }
            case PRODUCT_NOT_FOUND -> {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProductErrorInfo(productException.getProductError().getMessage()));
            }
            default -> {
                return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ProductErrorInfo("Other error"));
            }
        }
    }

}

package org.demo.productservice.exception.clothes;

import lombok.Getter;

@Getter
public enum ClothesError {
    NO_SUCH_CLOTHES("This clothing does not exist");


    private String message;

    ClothesError(String message) {
        this.message = message;
    }
}

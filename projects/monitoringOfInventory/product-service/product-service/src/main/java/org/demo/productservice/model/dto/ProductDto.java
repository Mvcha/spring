package org.demo.productservice.model.factory;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.demo.productservice.model.enumus.ProductCategory;

import java.time.LocalDate;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public abstract class ProductDto {
    private String productName;
    private Double productPrice;
    private String manufacturerName;
    private ProductCategory productCategory;
    private String createdDate;


    @Override
    public String toString() {
        return "ProductDto{" +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", manufacturerName='" + manufacturerName + '\'' +
                ", productCategory=" + productCategory +
                '}';
    }
}

package org.demo.productservice.exception.productException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ProductErrorInfo {
    private String message;


}

package org.demo.productservice.entity;

import jakarta.persistence.*;
import lombok.*;
import org.demo.productservice.model.enumus.ProductCategory;


@Getter
@Setter
@Entity
@Table(name = "products")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productId")
    private Long productId;
    @Column(name = "productName")
    private String productName;
    @Column(name = "productPrice")
    private Double productPrice;
    @Column(name = "manufacturerName")
    private String manufacturerName;
    @Enumerated(EnumType.STRING)
    @Column(name="productCategory")
    private ProductCategory productCategory;
    @Column(name="createdDate")
    private String createdDate;


/*    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="clothesId")
    private Clothes clothes;*/

}

package org.demo.productservice.exception.productException;

import lombok.Getter;

@Getter
public class ProductException extends RuntimeException{
    private ProductError productError;

    public ProductException(ProductError productError) {
        this.productError = productError;
    }
}

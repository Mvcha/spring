package org.demo.productservice.service.clothes;

import jakarta.transaction.Transactional;
import org.demo.productservice.entity.Clothes;
import org.demo.productservice.entity.ProductInfo;
import org.demo.productservice.entity.ProductMapper;
import org.demo.productservice.exception.clothes.ClothesError;
import org.demo.productservice.exception.clothes.ClothesException;
import org.demo.productservice.model.enumus.ProductCategory;
import org.demo.productservice.model.dto.ClothesDto;
import org.demo.productservice.model.dto.ProductDto;
import org.demo.productservice.model.factory.ProductFactory;
import org.demo.productservice.repository.ClothesRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ClothesServiceImpl implements ClothesService {

    private final ProductFactory productFactory;
    private final ProductMapper productMapper;
    private ClothesRepository clothesRepository;


    public ClothesServiceImpl(ProductFactory productFactory, ProductMapper productMapper, ClothesRepository clothesRepository) {
        this.productFactory = productFactory;
        this.productMapper = productMapper;
        this.clothesRepository = clothesRepository;
    }

    @Override
    @Transactional
    public void addClothes(ClothesDto clothesDto) {

/*
        ObjectMapper objectMapper = new ObjectMapper();
        //Map<String, Object> clothesProperties = objectMapper.convertValue(
                clothesDto, new TypeReference<Map<String, Object>>() {
                });
        //maybe use Java reflection?
*/

        ProductDto productDto = productFactory.createProduct(ProductCategory.CLOTHES, clothesDto);


        clothesRepository.save(productMapper.mapProductDtoToClothes(productDto));
    }

    @Override
    public Clothes findById(Long idClothes) {
        return clothesRepository.findById(idClothes)
                .orElseThrow(() -> new ClothesException(ClothesError.NO_SUCH_CLOTHES));
    }

    @Override
    @Transactional
    public void updateClothes(Long idClothes, ClothesDto clothesDto) {

        Clothes clothesFromDb = findById(idClothes);

        Clothes newClothes = productMapper.mapProductDtoToClothes(clothesDto);

        if (!newClothes.equals(clothesFromDb)) {
            ProductDto productDto = productFactory.createProduct(ProductCategory.CLOTHES, clothesDto);
            Clothes updatedClothes = productMapper.mapProductDtoToClothes(productDto);
            updatedClothes.setClothesId(clothesFromDb.getClothesId());
            ProductInfo productInfo = updatedClothes.getProductInfo();
            productInfo.setProductId(clothesFromDb.getClothesId());
            updatedClothes.setProductInfo(productInfo);
            clothesRepository.save(updatedClothes);

        } else {
            throw new ClothesException(ClothesError.CLOTHES_NOT_MODIFIED);
        }

    }

    @Override
    @Transactional
    public void deleteClothes(Long idClothes) {
        clothesRepository.delete(findById(idClothes));
    }

    public List<Clothes> getAllClothes() {
        return clothesRepository.findAll();
    }

}

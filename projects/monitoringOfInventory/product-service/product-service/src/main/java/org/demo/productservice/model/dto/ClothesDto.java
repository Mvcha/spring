package org.demo.productservice.model.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.demo.productservice.model.enumus.ClothesCategory;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ClothesDto extends ProductDto{


    private String productDescription;
    @NotBlank
    private String size;
    @NotNull
    private ClothesCategory clothesCategory;

    @Override
    public String toString() {
        return super.toString()+"ClothesProduct{" +
                "productDescription='" + productDescription + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    static ClothesDto createClothesProduct(ProductDto productDto)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.convertValue(productDto, ClothesDto.class);

    }
}

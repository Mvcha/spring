package org.demo.productservice.model.factory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ElectronicsDto extends ProductDto {
    private String modelName;
    private String serialNumber;

}

package org.demo.productservice.exception.productException;

import lombok.Getter;

@Getter
public enum ProductError {
    PRODUCT_ERROR("Other error"),
    PRODUCT_NOT_FOUND ("This product doesn't exist");

    private String message;

    ProductError(String message) {
        this.message=message;
    }
}

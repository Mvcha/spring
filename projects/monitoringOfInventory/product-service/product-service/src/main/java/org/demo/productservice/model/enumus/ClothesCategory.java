package org.demo.productservice.model.factory;

public enum ClothesCategory {
    JACKETS,
    SHIRTS,
    SHORTS,
    PANTS,
    BELT,
    HANDBAG,
    SNEAKERS
}

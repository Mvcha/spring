package org.demo.productservice.service.clothes;

import org.demo.productservice.entity.Clothes;
import org.demo.productservice.model.dto.ClothesDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ClothesService {
    List<Clothes> getAllClothes();
    void addClothes(ClothesDto clothesDto);
    Clothes findById(Long idClothes);
    void updateClothes(Long idClothes, ClothesDto clothesDto);
    void deleteClothes(Long idClothes);
}

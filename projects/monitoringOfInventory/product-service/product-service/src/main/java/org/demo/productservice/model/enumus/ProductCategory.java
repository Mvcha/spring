package org.demo.productservice.entity;

public enum ProductCategory {
    CLOTHES,
    ELECTRONICS,
    FURNITURE
}

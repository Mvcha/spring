package org.demo.productservice.service.products;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.demo.productservice.entity.Products;
import org.demo.productservice.model.enumus.ProductCategory;
import org.demo.productservice.exception.ProductError;
import org.demo.productservice.exception.ProductException;
import org.demo.productservice.model.factory.*;
import org.demo.productservice.repository.ClothesRepository;
import org.demo.productservice.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService{

    ProductRepository productRepository;
    ClothesRepository clothesRepository;

    public ProductServiceImpl(ProductRepository productRepository, ClothesRepository clothesRepository) {
        this.productRepository = productRepository;
        this.clothesRepository = clothesRepository;
    }

    @Override
    public List<Products> getProducts() {
        return productRepository.findAll();
    }

    @Override
    public Products getProduct(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ProductException(ProductError.PRODUCT_NOT_FOUND));
    }

    @Override
    public void addClothes(ClothesProduct clothesProduct) {

        ObjectMapper objectMapper = new ObjectMapper();
        Factory factory=new ProductFactory();
        Map<String, Object> clothesProperties = objectMapper.convertValue(
                clothesProduct, new TypeReference<Map<String, Object>>() {});
        //maybe use Java reflection?
      /*  clothesProperties.put("productName", clothesProduct.getProductName());
        clothesProperties.put("productPrice", clothesProduct.getProductPrice());
        clothesProperties.put("manufacturerName", clothesProduct.getManufacturerName());
        clothesProperties.put("size", clothesProduct.getSize());
        clothesProperties.put("productDescription", clothesProduct.getProductDescription());*/
        productRepository.save(factory.createProduct(ProductCategory.CLOTHES, clothesProperties))

    }
}

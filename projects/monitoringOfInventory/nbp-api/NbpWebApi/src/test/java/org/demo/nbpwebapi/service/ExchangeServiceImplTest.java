package org.demo.nbpwebapi.service;

import org.demo.nbpwebapi.model.ExchangeRatesSeries;
import org.demo.nbpwebapi.model.ExchangeRatesTable;
import org.demo.nbpwebapi.model.RateDetails;
import org.demo.nbpwebapi.model.RateTable;
import org.demo.nbpwebapi.nbpApi.NbpApi;
import org.demo.nbpwebapi.nbpApi.NbpApiImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ExchangeServiceImplTest {
    @Mock
    private WebClient webClient;
    private ExchangeRatesTable exchangeRatesTable;
    private ExchangeRatesSeries exchangeRatesSeries;
    private NbpApiImpl nbpApi;
    @InjectMocks
    private ExchangeServiceImpl exchangeService;

    @BeforeEach
    void setUp() {
        nbpApi = mock(NbpApiImpl.class);
        exchangeService = new ExchangeServiceImpl(nbpApi);
        exchangeRatesTable =
                new ExchangeRatesTable("A","046/A/NBP/2025", "2025-03-07", List.of(
                        new RateTable("bat (Tajlandia)", "THB", BigDecimal.valueOf(0.1143))));

        exchangeRatesSeries = new ExchangeRatesSeries("A", "dolar amerykański", "USD",
                List.of(new RateDetails("046/A/NBP/2025", "2025-03-07", BigDecimal.valueOf(3.8448))));
    }
    @Test
    void getAvailableCurrencies() {
        when(nbpApi.fetchRatesTable())
                .thenReturn(List.of(exchangeRatesTable));

        Map<String, String> currencies = exchangeService.getAvailableCurrencies();
        assertNotNull(currencies);
        assertEquals("bat (Tajlandia)", currencies.get("THB"));
    }
    @Test
    void getMidByCode() {
        when(nbpApi.fetchExchangeRatesSeries("USD"))
                .thenReturn(exchangeRatesSeries);
        assertEquals(BigDecimal.valueOf(3.8448), exchangeService.getMidByCode("USD"));
    }
}
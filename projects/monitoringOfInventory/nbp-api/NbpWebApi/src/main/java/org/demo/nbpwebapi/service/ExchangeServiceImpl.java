package org.demo.nbpwebapi.service;

import org.demo.nbpwebapi.mapper.RateMapper;
import org.demo.nbpwebapi.nbpApi.NbpApi;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.util.Map;

@Service
public class ExchangeServiceImpl implements ExchangeService {
    private final NbpApi nbpApi;

    public ExchangeServiceImpl(NbpApi nbpApi) {
        this.nbpApi = nbpApi;
    }

    @Cacheable("currencies")
    @Override
    public Map<String, String> getAvailableCurrencies() {
        return RateMapper.rateToMapCurrencies(nbpApi.fetchRatesTable().get(0).getRateTables());
    }

    @Cacheable("rateDetails")
    @Scheduled(fixedDelay = 100)
    @Override
    public BigDecimal getMidByCode(String code) {
        return nbpApi.fetchExchangeRatesSeries(code).getRateDetails().get(0).getMid();
    }


}

package org.demo.nbpwebapi.service;


import java.math.BigDecimal;
import java.util.Map;

public interface ExchangeService {
    Map<String,String> getAvailableCurrencies();

    BigDecimal getMidByCode(String code);
}

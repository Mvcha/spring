package org.demo.nbpwebapi.nbpApi;

import org.demo.nbpwebapi.model.ExchangeRatesSeries;
import org.demo.nbpwebapi.model.ExchangeRatesTable;

import java.util.List;

public interface NbpApi {
    List<ExchangeRatesTable> fetchRatesTable();

    ExchangeRatesSeries fetchExchangeRatesSeries(String code);
}

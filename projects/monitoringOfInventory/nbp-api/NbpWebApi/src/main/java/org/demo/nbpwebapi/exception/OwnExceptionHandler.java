package org.demo.nbpwebapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class OwnExceptionHandler {

    @ExceptionHandler(OwnException.class)
    public ResponseEntity<OwnException> ownException(OwnException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.errorInfo(e.getMessage(), e.getCode()));
    }

}

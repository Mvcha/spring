package org.demo.nbpwebapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@Setter
@AllArgsConstructor
public class RateTable {
    private String currency;
    private String code;
    private BigDecimal mid;

}

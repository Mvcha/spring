package org.demo.nbpwebapi.nbpApi;

import org.demo.nbpwebapi.exception.OwnException;
import org.demo.nbpwebapi.model.ExchangeRatesSeries;
import org.demo.nbpwebapi.model.ExchangeRatesTable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class NbpApiImpl implements NbpApi{
    private WebClient webClient;

    public NbpApiImpl(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public List<ExchangeRatesTable> fetchRatesTable() {
        List<ExchangeRatesTable> exchangeRatesTable = webClient.get()
                .uri("tables/A")
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, response ->
                        Mono.error(new OwnException("No rate found for this currency", 404))
                )
                .onStatus(HttpStatusCode::isError, response ->
                        Mono.error(new OwnException("Error when downloading data ",500))
                )
                .bodyToMono(new ParameterizedTypeReference<List<ExchangeRatesTable>>() {
                }).block();

        if (exchangeRatesTable == null || exchangeRatesTable.isEmpty()) {
            throw new OwnException("No rate found for this currency", 404);
        }

        return exchangeRatesTable;
    }

    @Override
    public ExchangeRatesSeries fetchExchangeRatesSeries(String code) {
        ExchangeRatesSeries exchangeRatesSeries = webClient.get()
                .uri("rates/A/" + code)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, response ->
                        Mono.error(new OwnException("No rate found for this currency", 404))
                )
                .onStatus(HttpStatusCode::isError, response ->
                        response.createException()
                                .flatMap(error -> Mono.error(new OwnException("Error when downloading data ",500)))
                )
                .bodyToMono(ExchangeRatesSeries.class)
                .block();

        return exchangeRatesSeries;
    }
}

package org.demo.nbpwebapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@ToString
@AllArgsConstructor
public class RateDetails {
    private String no;
    private String effectiveDate;
    private BigDecimal mid;
}

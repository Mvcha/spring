package org.demo.nbpwebapi.controller;


import org.demo.nbpwebapi.service.ExchangeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/exchange")
public class ExchangeController {


    private final ExchangeService exchangeService;

    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @GetMapping("/currencies")
    public ResponseEntity<Map<String,String>> getAvailableCurrencies()
    {
        return ResponseEntity.ok(exchangeService.getAvailableCurrencies());
    }
    @GetMapping("/{code}")
    public ResponseEntity<BigDecimal> getMidByCode(@PathVariable String code)
    {
        return ResponseEntity.ok(exchangeService.getMidByCode(code));
    }

}

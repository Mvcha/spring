package org.demo.nbpwebapi.mapper;

import org.demo.nbpwebapi.model.RateTable;
import org.demo.nbpwebapi.model.dto.Currencies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RateMapper {

    public static Currencies rateToRateDto(RateTable rateTable)
    {
        return new Currencies(rateTable.getCurrency(), rateTable.getCode());
    }
    public static List<Currencies> rateListToRateDtoList(List<RateTable> rateTable)
    {
        return rateTable.stream()
                .map(rateTable1 -> new Currencies(rateTable1.getCurrency(), rateTable1.getCode()))
                .collect(Collectors.toList());
    }

    public static Map<String, String> rateToMapCurrencies(List<RateTable> rateTable)
    {
        return rateTable.stream()
                .collect(Collectors.toMap(currencies ->currencies.getCode(), currencies ->currencies.getCurrency() ));
    }

}

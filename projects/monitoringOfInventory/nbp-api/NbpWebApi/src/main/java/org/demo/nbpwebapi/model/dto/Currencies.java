package org.demo.nbpwebapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class Currencies {
    private String currency;
    private String code;

    private Map<String, String> currencies;

    public Currencies(String currency, String code) {
        this.currency = currency;
        this.code = code;
    }

}

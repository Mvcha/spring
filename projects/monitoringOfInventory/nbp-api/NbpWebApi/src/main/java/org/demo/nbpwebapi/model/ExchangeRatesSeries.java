package org.demo.nbpwebapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ExchangeRatesSeries {
    private String table;
    private String currency;
    private String code;
    @JsonProperty("rates")

    private List<RateDetails> rateDetails;
}

package org.demo.nbpwebapi.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties({"stackTrace", "cause", "localizedMessage", "suppressed"})
public class OwnException extends RuntimeException{
    private String message;
    private int code;

    public OwnException errorInfo(String message, int code) {
        return new OwnException(message, code);
    }

}
